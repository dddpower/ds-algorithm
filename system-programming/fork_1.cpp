#define DEBUG
#ifdef DEBUG
#include <bits/stdc++.h>
#include <unistd.h>
using namespace std;
#endif

#ifdef DEBUG
int main() {
    auto pid = fork();
    if (pid)
        printf("parent\n");
    else
        printf("child\n");
}
#endif
