#include <stdio.h>
#define MAX_N 101
#define MAX_CAP 100001
#define INPUT(X) scanf("%d", &X)
#define PRINT(X) printf("%d\n", X)
int N, capacity;
int weight[MAX_N];
int value[MAX_N];
int mem[MAX_N][MAX_CAP];
void input() {
  INPUT(N);
  INPUT(capacity);
  for (int i = 1; i <= N; ++i) {
    INPUT(weight[i]);
    INPUT(value[i]);
  }
}
int max2(int a, int b) {
  return a < b ? b : a;
}
int knapsack_ordinary(int n, int w) {
  if (!n) {
    return 0;
  }
  int* val = &mem[n][w];
  if (!*val) {
    if (w - weight[n] >= 0) {
      *val = max2(value[n] + knapsack_ordinary(n - 1, w - weight[n]),
            knapsack_ordinary(n - 1, w));
    } else {
      *val = knapsack_ordinary(n - 1, w);
    }
  }
  return *val;
}
int main() {
  input();
  int answer = knapsack_ordinary(N, capacity);
  printf("%d\n", answer);
}
