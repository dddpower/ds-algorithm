#include <iostream>
#include <cstdio>
#include <assert.h>
#include <array>
using namespace std;

int N;//버섯 수
int P[150000 + 10];//버섯 값
array<int, 150000 +10> A;

void InputData(){
	cin >> N;
	for (int i = 0; i < N; i++){
		cin >> P[i];
		A[i] = P[i];
	}
}
int Solve_C() {
	int sum = 0;
	int p, c = 0;
	for (int i = 0; i < N; ++i) {
		p = c;
		c = P[i];
		sum += p < c ? c - p : 0;
	}
	return sum;
}
int Solve_Modern() {
	// auto A = to_array(P);
	int sum = 0;
	int p, c = 0;
	for (auto it = A.begin(); it != A.end(); ++it) {
		p = c;
		c = *it;
		sum += p < c ? c - p : 0;
	}
	return sum;
}

int main(){
	int ans = -1;
	InputData();			//	입력 함수

	ans = Solve_Modern();
	
	
	cout << ans << endl;	//	정답 출력
	return 0;
}
