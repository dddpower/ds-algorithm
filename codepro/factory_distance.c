#include <stdio.h>
#include <limits.h>
#define MIN(X, Y) X < Y ? X : Y
#define MAX(X, Y) X > Y ? X : Y
#define SIZE 5000
#define MY_MAX 9999
int N, M;//공장 수, 도로 정보 수
int A[SIZE], B[SIZE], D[SIZE];//공장 A, 공장 B, 거리 D
int graph[SIZE +1][SIZE + 1];

void InputData(){
  int i;
  
  scanf("%d %d", &N, &M);
  for (i = 0; i < M; i++){
    scanf("%d %d %d", &A[i], &B[i], &D[i]);
    graph[A[i]][B[i]] = D[i];
    graph[B[i]][A[i]] = D[i];
		//printf("d[i] = %d\n", D[i]);
  }
}
int main(){
  int ans = -1;
  // initialize to INF
  for (int i = 1; i <= SIZE; ++i) {
    for (int j = 1; j <= SIZE; ++j) {
      graph[i][j] = MY_MAX;
    }
  }
	
  for (int i = 1; i <= SIZE; ++i) {
    graph[i][i] = 0;
  }
  
  InputData();//	입력 함수

  //	코드를 작성하세요
  // floyd-warshall's algorithm
	printf("initial graph\n");
	for (int i = 1; i <= N; ++i) {
		for (int j = 1; j <= N; ++j) {
			printf("%d ", graph[i][j]);
		}
		printf("\n");
	}
	printf("\n");
  for (int i = 1; i <= N; ++i) {
    for (int j = 1; j <= N; ++j) {
      for (int k = 1; k <= N; ++k) {
        int*a = &graph[j][k];
        int*b = &graph[j][i];
        int*c = &graph[i][k];
       
				if (*b != MY_MAX && *c != MY_MAX) {
					//printf("a = %d, b = %d, c = %d\n", *a, *b, *c);	
				  *a = MIN(*a, *b + *c);
					//printf("fixed a = %d\n", *a);
        }
      }
    }
  }
	printf("result\n");
	for (int i = 1; i <= N; ++i) {
		for (int j = 1; j <= N; ++j) {
			printf("%d ", graph[i][j]);
		}
	
		printf("\n");
	}
	
  int max = 0, min_max = MY_MAX;
  for (int i = 1; i <= N; ++i) {
    for (int j = 1; j <= N; ++j) {	
      max = MAX(max, graph[i][j]);
    }
    min_max = MAX(min_max, max);
  }
  ans = min_max;

  printf("%d\n", ans);//	정답 출력
  return 0;
}


