#include <bits/stdc++.h>
using namespace std;

int N;//후보자 수
int A[100000 + 10];//기질 값

void InputData(){
	int i;
	scanf("%d", &N);
	for (i = 0; i < N; i++) scanf("%d", &A[i]);
}
pair<int, int> solve() {
    int first = A[0];
    int second = A[1];
    int f_i = 0, s_i = 1;
    for (int i = 2; i < N; ++i) {
        int current = A[i];
        int val1 = abs(first + second);
        int val2 = abs(first + current);
        int val3 = abs(second + current);
        if (val2 < val3 && val2 < val1) {
            second = current;
            s_i = i;
        }
        if (val3 < val2 && val3 < val1) {
            first = current;
            f_i = i;
        }
    }
	assert (f_i != s_i);
    if (f_i > s_i)
        swap(f_i, s_i);
    return make_pair(f_i, s_i);
}
template<typename T>
void print_pair(pair<T, T> p) {
    cout << p.first << " " << p.second << endl;
}
int main(){
	InputData();//	입력 함수
    auto ans = solve();
    reverse(begin(A), end(A));
    auto ans2 = solve();
    if (ans.first < ans2.first)
        print_pair(ans);
    else
        print_pair(ans2);
}
