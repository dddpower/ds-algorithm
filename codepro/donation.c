#include <stdio.h>
int N;
int M;
int A[1000010];

int input_data(void){
	int i, max = 0;
	scanf("%d %d", &N, &M);
	for (i = 0; i < N; i++){
		scanf("%d", &A[i]);
		if (max < A[i]) max = A[i];
	}
	return max;
}
int check(int h){
	int i, sum = M;
	for (i = 0; i < N; i++){
		if (h < A[i]) {
			sum -= A[i] - h;
			if (sum <= 0) return 1;
		}
	}
	return 0;
}
int solve(int max){
	int s = 0, e = max, m, sol = 0;
	while (s <= e){
		m = (s + e) / 2;
		if (check(m) == 1){
			sol = m; s = m + 1;
		}
		else e = m - 1;
	}
	return sol;
}
int main(void){
	int max = input_data();
	int ans = solve(max);
	printf("%d\n", ans);
	return 0;
}
