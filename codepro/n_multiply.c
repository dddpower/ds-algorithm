#include <stdbool.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#define SIZE 110
#define ASCIINUM 55
#define NEWLINE printf("\n")
int n, count;
int a_len, b_len;
bool minus;
char a[SIZE], b[SIZE], tmp[SIZE];
int result[2 * SIZE][2 * SIZE];
int ans[2 * SIZE];
bool zerocan;

void print_result() {
  for (int i = 0; i < a_len; ++i) {
    for (int j = 0; j < a_len + b_len - 1; ++j) {
      printf("%d\t", result[i][j]);
    }
    printf("\n");
  }
}

char answer_str[SIZE * 2];
int ans_i;
void save_single(int x) {
  if (x > 9) {
    // printf("%c", x + ASCIINUM);
    answer_str[ans_i++] = x + ASCIINUM;
    zerocan = true;
  } else {
    if (!x) {
      if (zerocan) {
        answer_str[ans_i++] = '0';
        // printf("0");
      }
    } else {
      zerocan = true;
      answer_str[ans_i++] = x + '0';
      // printf("%d", x);
    }
  }
}
void print_ans(int c) {
  
  if (c) {
    save_single(c);
  }
  for (int i = 0; i < a_len + b_len - 1; ++i) {
    save_single(ans[i]);
  }
  
  // NEWLINE;
  if (zerocan && minus) {
    printf("-");
  }
  for (int i = 0; i < ans_i; ++i) {
    printf("%c", answer_str[i]);
  }
  if (!zerocan) {
    printf("0");
  }
  printf("\n");
}



void clear_ans() {
  for (int i = 0; i < 2* SIZE; ++i) {
    ans[i] = 0;
  }
}
void clear_result() {
  for (int i = 0; i < 2 * SIZE; ++i) {
    for (int j = 0; j < 2 * SIZE; ++j) {
      result[i][j] = 0;
    }
  }
  zerocan = false;
}
void modify() {
  a_len = strlen(a);
  a_len -= (a[0] == '-' ? 1 : 0);
  b_len = strlen(b);
  b_len -= (b[0] == '-' ? 1 : 0);

  if (a[0] == '-') {
    if (b[0] == '-') {
      minus = false;
    } else {
      minus = true;
    }
  } else {
    if (b[0] == '-') {
      minus = true;
    } else {
      minus = false;
    }
  }

  if (a[0] == '-') {
    strcpy(tmp, &a[1]);
    strcpy(a, tmp);
  }
  if (b[0] == '-') {
    strcpy(tmp, &b[1]);
    strcpy(b, tmp);
  }
  // printf("preprocess result");
  // NEWLINE;
  // printf("a = %s\n", a);
  // printf("b = %s\n", b);
  // printf("minus = %d\n", minus);
  // NEWLINE;

  for (int i = 0; i < a_len; ++i) {
      a[i] -= a[i] >= 'A' ? ASCIINUM : '0';
    
  }
  for (int i = 0; i < b_len; ++i) {
      b[i] -= b[i] >= 'A' ? ASCIINUM : '0';
  }

  // for (int i = 0; i < a_len; ++i) {
  //   printf("%d ", a[i]);
  // }
  // NEWLINE;
  // for (int i = 0; i < b_len; ++i) {
  //   printf("%d ", b[i]);
  // }
  // NEWLINE;
  // NEWLINE;
}
void calculate() {
  for (int i = 0; i < a_len; ++i) {
    for (int j = 0; j < b_len; ++j) {
      result[i][j + i] = a[i] * b[j];
    }
  }
  // printf("print result\n");
  // print_result();
  // gather whole to the first row
  for (int i = 0; i < a_len + b_len - 1; ++i) {
    int sum = 0;
    for (int j = 0; j < a_len; ++j) {
      sum += result[j][i];
    }
    ans[i] = sum;
  }

  // carry
  int c = 0;
  for (int i = a_len + b_len - 2; i >= 0; --i) {
    ans[i] += c;
    c = ans[i] / n;
    ans[i] %= n;
  }

  // NEWLINE;
  print_ans(c);
  // NEWLINE;
}

int main() {
  // FILE* fp = fopen("input.txt", "r");
  // fscanf(fp, "%d", &count);
  scanf("%d", &count);
  for (int i = 0; i < count; ++i) {
    // fscanf(fp, "%d %s %s", &n, a, b);
    scanf("%d %s %s", &n, a, b);
    modify();
    calculate();
    clear_result();
    ans_i = 0;
  }
  // fclose(fp);
}
