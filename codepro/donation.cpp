#include <bits/stdc++.h>
using namespace std;

int N; 
int M; 
int A;
vector<int> persons;

void Input_Data(void) {
	int i;
	scanf("%d %d", &N, &M);
	for (i = 0; i < N; i++){
		scanf("%d", &A);
    persons.push_back(A);
	}
}

int gather_money(int a) {
  int sum = 0;
  for (auto it = persons.begin(); it != persons.end(); ++it) {
    int var = *it - a;
    sum += var > 0 ? var : 0;
  }
  // printf("gather money result = %d\n", sum);
  return sum;
}

int answer() {
  // sort
  sort(persons.begin(), persons.end());

  // get min & max;
  int min = INT_MAX;
  int max = 0;
  for (auto it = persons.begin(); it != persons.end(); ++it) {
    min = min > *it ? *it : min;
    max = max < *it ? *it : max;
  }
  int l = min;
  int r = max;
  int c = -1;
  int p;
  int v;
  do {
    p = c;
    c = (l + r) / 2;
    v = gather_money(c);
    if (v == M) {
      return c;
    } else {
      if (v < M) {
        // to less
        r = c;
      } else {
        // to much
        l = c;
      }
    }
  } while (c != p);

  if (v > M) {
    return c;
  } else {
    // v < M
    while (v < M) {
      v = gather_money(--c);
    }
    return c;
  }
}

int main(void) {
	int ans = 0;
	Input_Data();
	ans = answer();
	
	printf("%d\n", ans);
	return 0;
}

