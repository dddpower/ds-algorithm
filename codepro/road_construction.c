#include <stdio.h>
#include <stdbool.h>
#define SIZE 110
#define QUEUESIZE (SIZE * SIZE)
#define INF 999999
char map[SIZE][SIZE];
int score[SIZE][SIZE];
int N;

typedef struct _point {
  int x, y;
} point;

point dir_1(point p) {
  ++p.x;
  return p;
}
point dir_2(point p) {
  --p.x;
  return p;
}
point dir_3(point p) {
  ++p.y;
  return p;
}
point dir_4(point p) {
  --p.y;
  return p;
}
bool inbound_single(int x) {
  return x >= 0 && x < N;
}
bool inbound(point p) {
  return inbound_single(p.x) && inbound_single(p.y);
}
point queue[QUEUESIZE];
int front = 0, rear = 0;
bool isEmpty() {
  return front == rear;
}
int increase_i(int i) {
  return (i + 1) % QUEUESIZE;
}
bool isFull() {
  return increase_i(rear) == front;
}
void push(point p) {
  if (isFull()) {
    printf("error: queue is full\n");
		printf("front = %d, rear = %d, increase_i(rear) = %d\n", front, rear, increase_i(rear));
    return;
  } 
  queue[rear] = p;
  rear = increase_i(rear);
  printf("(%d, %d) pushed\n", p.x, p.y);
}

point pop() {
  point p;
  if (isEmpty()) {
    printf("error : queue is Empty\n");
  } else {
    p = queue[front];
    front = increase_i(front);
  }
  return p;
}
int getMap(point p) {
  return map[p.x][p.y] - '0';
}
int getScore(point p) {
  return score[p.x][p.y];
}
void setScore(point p, int value) {
  score[p.x][p.y] = value;
	printf("score[%d][%d] = %d\n", p.x, p.y, value);
}
int Solve() {
  //init score to INF
  for (int i = 0; i < N; ++i) {
    for (int j = 0; j < N; ++j) {
      score[i][j] = INF;
    }
  }

  point initial_p;
  initial_p.x = 0;
  initial_p.y = 0;
  setScore(initial_p, 0);
  push(initial_p);
  while (!isEmpty()) {
    point p = pop();
    point p_1 = dir_1(p); 
    point p_2 = dir_2(p);
    point p_3 = dir_3(p);
    point p_4 = dir_4(p);
    int new_score;
    if (inbound(p_1)) {
      new_score = getScore(p) + getMap(p_1);
      if (getScore(p_1) > new_score) {
        setScore(p_1, new_score);
        push(p_1);
      }
    }
    if (inbound(p_2)) {
      new_score = getScore(p) + getMap(p_2);
      if (getScore(p_2) > new_score) {
        setScore(p_2, new_score);
        push(p_2);
      }
    }
    if (inbound(p_3)) {
      new_score = getScore(p) + getMap(p_3);
      if (getScore(p_3) > new_score) {
        setScore(p_3, new_score);
        push(p_3);
      }
    }
    if (inbound(p_4)) {
      new_score = getScore(p) + getMap(p_4);
      if (getScore(p_4) > new_score) {
        setScore(p_4, new_score);
        push(p_4);
      }
    }
  }
  return score[N-1][N-1];
}
void Input_Data(void){
  int i;
  scanf("%d", &N);
  for(i = 0 ; i < N ; i++){
    scanf("%s", map[i]);
  }
}
int main(void){ 
  int ans = -1;
  Input_Data();		
  ans = Solve();
  printf("%d\n", ans);
  return 0;
}


