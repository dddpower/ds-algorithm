#include <stdio.h>
#define MAX 100001
#define ITEM 10

int N, M;
int source[MAX];
int permute_array[ITEM];
int sum[ITEM][MAX]; 
void input_and_gen() {
  int val;
  scanf("%d %d", &N, &M);
  //warning
  for (int i = 0; i < N; ++i) {
    scanf("%d", &val);
    source[i] = val;
  }
  
  for (int i = 0; i < M; ++i) {
    permute_array[i] = i + 1;
  }
  for (int i = 1; i <= M; ++i) {
    for (int j = 0; j < N; ++j) {
      sum[i][j] = (j - 1 >= 0 ? sum[i][j - 1] : 0) + (i == source[j] ? 1 : 0);
    }
  }
}
 
void swap(int *a, int *b) {
  int v = *a;
  *a = *b;
  *b = v;
}

// Generating permutation using Heap Algorithm 
int how_many(int n) {
  return sum[n][N - 1];
}
int source_count(int n, int from, int to) {
  return from - 1 != 0 ? sum[n][to] - sum[n][from - 1] : sum[n][to];
}
int Calculate() {
  int diff = 0;
  int from = 0, to;
  for (int i = 0; i < M; ++i) {
    int count = how_many(permute_array[i]);
    to = from + count - 1;
    diff += count - source_count(permute_array[i], from, to);
    from = to + 1;
  }
  return diff;
}
int min = MAX + 1;
void Solve_ANSI_C(int size, int n) 
{ 
    // if size becomes 1 then prints the obtained 
    // permutation 
    if (size == 1) 
    { 
       int diff = Calculate();
       min = min < diff ? min : diff;
       return;
    } 
  
    for (int i=0; i<size; i++) 
    { 
        Solve_ANSI_C(size-1,n); 
  
        // if size is odd, swap first and last 
        // element 
        if (size%2==1) 
            swap(&permute_array[0], &permute_array[size-1]); 
  
        // If size is even, swap ith and last 
        // element 
        else
            swap(&permute_array[i], &permute_array[size-1]); 
    } 
} 
void Solve_CPP(int size, int n) {

}

int main() {
  input_and_gen();
    
  Solve_ANSI_C(M, M);
  printf("%d\n", min);
}
