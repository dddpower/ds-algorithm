#include <stdio.h>

int euclid(int m, int n) {
  if (!n) {
    return m;
  }

  if (!m) {
    return n;
  }
  
  int remain = m % n;
  if (!remain) {
    return n;
  }

  return euclid(n, remain);
}

int main() {
  int m, n;
  scanf("%d %d", &m, &n);
  int answer = euclid(m, n);
  printf("%d\n", answer);
}
