#include <stdio.h>
#include <stdbool.h>
#include <stdio.h>

// started at 0917
int N;
int X, Y;
int map[20][20];
bool visit[20][20];
void Input_Data(void){
	int i, j;
	scanf("%d", &N);
	scanf("%d %d", &X, &Y);
	for(i=0 ; i<N ; i++){
		for(j=0 ; j<N ; j++){
			scanf("%1x", &map[i][j]);
		}
	}
}
typedef struct _point {
  int x;
  int y;
} point;
int getMap(point p) {
  return map[p.y][p.x];
}
bool getVisit(point p) {
  return visit[p.y][p.x];
}
int getPipe(int map_val) {
  int pipe[12] = {0b0000, 0b0011, 0b1100, 0b0101, 0b0110, 0b1010,
                  0b1001, 0b1101, 0b0111, 0b1110, 0b1011, 0b1111};
  return pipe[map_val];
}
point left(point p) {
  --p.x;
  return p;
}
point right(point p) {
  ++p.x;
  return p;
}
point up(point p) {
  --p.y;
  return p;
}
point down(point p) {
  ++p.y;
  return p;
}
bool outbound_single(int x) {
  return x < 0 || x >= N;
}
bool outbound(point p) {
  return outbound_single(p.x) || outbound_single(p.y);
}

bool leftExist(point p) {
  return getPipe(getMap(p)) & 0b0010;
}
bool rightExist(point p) {
  return getPipe(getMap(p)) & 0b0001;
}
bool upExist(point p) {
  return getPipe(getMap(p)) & 0b1000;
}
bool downExist(point p) {
  return getPipe(getMap(p)) & 0b0100;
}
void DFS(point p) {
  // Assume point p is always a new visit.
  // printf("visit (%d, %d)\n", p.x, p.y);
  visit[p.y][p.x] = true;

  point left_p = left(p);
  point right_p = right(p);
  point up_p = up(p);
  point down_p = down(p);

  if (!getVisit(left_p) && !outbound(left_p) &&
      leftExist(p) && rightExist(left_p)) {
    DFS(left_p);
  }
  if (!getVisit(right_p) && !outbound(right_p) &&
      rightExist(p) && leftExist(right_p)) {
    DFS(right_p);
  }
  if (!getVisit(up_p) && !outbound(up_p) &&
      upExist(p) && downExist(up_p)) {
    DFS(up_p);
  }
  if (!getVisit(down_p) && !outbound(down_p) &&
      downExist(p) && upExist(down_p)) {
    DFS(down_p);
  }
}


int main(void) {
	int ans = -1;
	Input_Data();	
  point initial_p;
  initial_p.x = X;
  initial_p.y = Y;
  DFS(initial_p);
  // 제거 = 전체 파이프 - 방문 파이프
  int count = 0;
  for (int i = 0; i < N; ++i) {
    for (int j = 0; j < N; ++j) {
      if (visit[i][j] || map[i][j] == 0) {
        ++count;
      }    
    }
  }
  ans = N * N - count;
	printf("%d\n", ans);		//	정답 출력
	return 0;
}

