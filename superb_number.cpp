#include <bits/stdc++.h>
using namespace std;
int num(char ch) {
  return ch - '0';

}
char ch(int n) {
  return n + '0';

}
// Answer of better solution. This can print be printed like big integer
// by using string return type.
string solve_string(int k) {
  queue<string> q{};
  q.push("1");
  q.push("2");
  q.push("3");
  q.push("4");
  q.push("5");
  q.push("6");
  q.push("7");
  q.push("8");
  q.push("9");

  string val{""};
  while (k--) {
    val = q.front();
    q.pop();
    int last_num = num(*(val.end() - 1));
    if (last_num == 0) {
      q.push(val.append("1"));

    } else if (last_num == 9) {
      q.push(val.append("8"));

    } else {
      q.push(val + ch(last_num - 1));
      q.push(val + ch(last_num + 1));
    }
  }
  return val;
}

// first solution
// bfs search algorithm
// time complexity O(N)

int solve(int k) {
  queue<int> q{};
  q.push(1);
  q.push(2);
  q.push(3);
  q.push(4);
  q.push(5);
  q.push(6);
  q.push(7);
  q.push(8);
  q.push(9);
  size_t val{0};
  while (k--) {
    val = q.front();
    q.pop();
    int last_num = val % 10;
    if (last_num == 0) {
      q.push(val * 10 + 1);
    } else if (last_num == 9) {
      q.push(val * 10 + 8);
    } else {
      q.push(val * 10 + last_num - 1);
      q.push(val * 10 + last_num + 1);
    }
  }
  return val;
}
int main() {
  int k;
  cin >> k;
  int val = solve(k);
  cout << val << endl;
}
