#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>
#include <time.h>
#define MAX 1000
int top = -1;
int heap[MAX];
bool isFull() {
  return top == MAX - 1;
}
bool isEmpty() {
  return top == -1;
}
int parent_i(int i) {
  if (i)
    return (i + 1) / 2 - 1;
  // root
  return -1;
}
int l_child_i(int i) {
  return 2 * i + 1;
}
//right_child_i = left_child_i + 1

void swap(int *a, int *b) {
  int temp = *a;
  *a = *b;
  *b = temp;
}
// update ith element to val
bool update(int i, int val) {
  heap[i] = val;

}
bool heapify(int i) {
  return true;
}
bool insert(int val) {
  if (isFull())
    return false;
  heap[++top] = val;
  int i = top;
  while (i > 0) {
    int p_i = parent_i(i);
    if (heap[p_i] > heap[i]) {
      swap(&heap[p_i], &heap[i]);
      i = p_i;
    } else {
      break;
    }
  }
  return true;
}

int pop() {
  if (isEmpty())
    return -1;
  int i = 0;
  int l_i = 1;
  int r_i = 2;
  int return_val = heap[i];
  while (l_i < top) {
    if (heap[l_i] < heap[r_i]) {
      swap(&heap[i], &heap[l_i]);
      i = l_i;
    } else {
      swap(&heap[i], &heap[r_i]);
      i = r_i;
    }
    l_i = l_child_i(i);
    r_i = l_i + 1;
  }
  heap[i] = heap[top--];
  return return_val;
}

void print_heap() {
  int newlinecount = 0;
  int twos_multiply = 1;
  for (int i = 0; i <= top; ++i) {
    printf("%d ", heap[i]);
    ++newlinecount;
    if (newlinecount == twos_multiply) {
      printf("\n");
      twos_multiply *= 2;
      newlinecount = 0;
    }
  }
  printf("\n");
}
int main () {
} 
