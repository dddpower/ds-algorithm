#include <bits/stdc++.h>
using namespace std;
class Solution {
public:
    int minSetSize(vector<int>& arr) {
        map<int, int> m;
        for (auto i = arr.begin(); i != arr.end(); ++i) {
            ++m[*i];
        }
        vector<pair<int ,int>> v(m.begin(), m.end());
        sort(m.begin(), m.end(), less);
    }
};
