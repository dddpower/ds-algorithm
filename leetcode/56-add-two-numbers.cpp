#include <bits/stdc++.h>
using namespace std;
// Definition for singly-linked list.
struct ListNode {
    int val;
    ListNode *next;
    ListNode() : val(0), next(nullptr) {}
    ListNode(int x) : val(x), next(nullptr) {}
    ListNode(int x, ListNode *next) : val(x), next(next) {}
};
class Solution {
	public:
		ListNode* addTwoNumbers(ListNode* l1, ListNode* l2) {
			if (!l1)
				return l2;
			if (!l2)
				return l1;
			auto val = div(l1->val + l2->val, 10);
			ListNode* head = new ListNode{val.rem};
			ListNode* l{head};
			l1 = l1->next;
			l2 = l2->next;
			while (l1 && l2)
			{
				val = div(val.quot + l1->val +l2->val, 10);
				l->next = new ListNode(val.rem);
				l = l->next;
				l1 = l1->next;
				l2 = l2->next;
			}
			auto code_block = [&l, &val](ListNode *x)->void {
				while (x) {
					val = div(val.quot + x->val, 10);
					l->next = new ListNode{val.rem};
					l = l->next;
					x = x->next;
				}
			};
			code_block(l1);
			code_block(l2);
			if (val.quot) {
				l->next = new ListNode{1};
			}
			return head;
		}
};
