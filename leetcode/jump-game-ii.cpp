#include <bits/stdc++.h>
using namespace std;
class Solution {
  public:
    int jump(vector<int>& nums) {
      auto length = nums.size();
      size_t reach{0};
      size_t step = 0;
      size_t i = 0;
      size_t max{0};
      while(true) {
        while(i <= reach) {
          if (reach >= length - 1) {
            return step;
          }
          auto val = i + nums[i];
          max = max > val ? max : val;
          ++i;
        }
        i = reach + 1;
        reach = max;
        ++step;
      }
      // something is wrong
      return 0;
    }
};
int main() {

}
