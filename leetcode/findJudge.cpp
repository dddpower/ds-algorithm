#define DEBUG
#ifdef DEBUG
#include <bits/stdc++.h>
using namespace std;

#endif

class Solution {
public:
    int findJudge(int N, vector<vector<int>>& trust) {
        vector<int> t_count{};
        t_count.resize(N + 1);
        t_count.shrink_to_fit();
        vector<bool> believe{};
        believe.resize(N + 1);
        believe.shrink_to_fit();
        for (vector<int>& tup : trust) {
            believe[tup[0]] = true;
            ++t_count[tup[1]];
        }
        bool no_trust = false;
        for (int i = 1 ; i <= N; ++i) {
            if (t_count[i] == N - 1 && !believe[i])
                return i;
        }
        return -1;
    }
};

#ifdef DEBUG
int main() {

}
#endif
