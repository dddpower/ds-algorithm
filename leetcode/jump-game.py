from typing import List
class Solution:
    def jmp(self, nums: List[int], visit: List[bool], fin, cur) -> bool:
        if visit[cur] == True:
            return False
        visit[cur] = True
        val = nums[cur]
        if cur == fin:
            return True
        if val == 0:
            return False
        if cur + val >= fin:
            return True
        for i in range(1, val + 1):
            next_val = cur + i
            result = self.jmp(nums, visit, fin, cur + i)
            if result == True:
                return True
        return False

    def canJump(self, nums: List[int]) -> bool:
        length = len(nums)
        visit = [False] * length
        return self.jmp(nums, visit, length - 1, 0)
    
