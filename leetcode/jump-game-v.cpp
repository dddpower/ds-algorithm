#include <bits/stdc++.h>
using namespace std;
class Solution {
  public:
    size_t roof_index(const vector<int>& arr, size_t start, bool right, int d) {
      pair<size_t, size_t> max_p{0,0}; // <max, index>
      int inc = right ? 1 : -1;
      return max_p.second;
    }
    int maxJumps(vector<int>& arr, int d) {
      auto length = arr.size();
      if (length == 0 || length == 1)
        return 0;
      if (length == 2) {
        return 1;
      }
      if (d >= length - 1)
        return length - 1;
      auto high = roof_index()
      // unreachable
      return 0;
    }
};

int main() {
  // 6, 4, 14, 6, 8, 13, 9, 7, 10, 6, 12
  vector<int> test{6, 4, 14, 6};
  Solution s{};
  auto result = s.maxJumps(test, 2);
  cout << result << endl;
}
