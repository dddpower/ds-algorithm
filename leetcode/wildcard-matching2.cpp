#define DEBUG

#ifdef DEBUG
#include <bits/stdc++.h>
using namespace std;
#endif
class Solution {
    public:
        bool isMatch(string s, string p) {          
            auto row_size = s.size() + 1;
            auto col_size = p.size() + 1;
            vector<vector<bool>> t{};
            t.resize(row_size);
            t.shrink_to_fit();
            for (auto& each_row : t) {
                each_row.resize(col_size);
                each_row.shrink_to_fit();
            }
            t[0][0] = true;
            for (size_t j = 1; j < col_size; ++j) {
                if (p[j - 1] == '*') {
                    t[0][j] = t[0][j - 1];
                }
            }
            
            for (size_t i = 1; i < row_size; ++i) {
                for (size_t j = 1; j < col_size; ++j) {
                    if (p[j - 1] == '*') {
                        t[i][j] = t[i][j - 1] || t[i - 1][j] || t[i - 1][j - 1];

                    } else if (p[j - 1] == '?' || p[j - 1] == s[i - 1]) {
                        t[i][j] = t[i - 1][j - 1];
                    } 
                }
            }
            return t[row_size - 1][col_size - 1];
        }
};


#ifdef DEBUG
int main() {
    Solution s{};
    cout << s.isMatch("aa", "a") << endl;
    cout << s.isMatch("aa", "*") << endl;
    cout << s.isMatch("cb", "?a") << endl;
    cout << s.isMatch("abceb", "*a*b") << endl;
    cout << s.isMatch("acdcb", "a*c?b") << endl;
}
#endif

