#define LEETCODE
#ifdef LEETCODE
#include <bits/stdc++.h>
using namespace std;
#endif
class Solution {
  int count{0};
  public:
  int solve(vector<int>&coins, int amount, vector<int>& mem, int i) {
    return 0;
  }
  int change(int amount, vector<int>& coins) {
    if (amount == 0 && begin(coins) == end(coins))
      return 1;
    vector<int> m{};
    m.resize(amount + 1);
    m.shrink_to_fit();
    return solve(coins, amount, coins.size() - 1, m, 0);
  }
};
#ifdef LEETCODE
int main() {
  Solution s;
  vector<int> v = {1, 2, 5};
  auto result = s.change(5, v);
  cout << result;
}
#endif
