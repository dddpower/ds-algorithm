#include <bits/stdc++.h>
using namespace std;
class Solution {
public:
    int findIndex(vector<int>& nums, int left, int right) {
        // to find rotated point, only search range which 
        // satisfies nums[left] > nums[right]
        // always left <= right
        int m = left + (right - left) / 2;
        if (nums[m] > nums[m + 1]) {
            return m;
        }
        if (nums[m] > nums[right]) {
            return findIndex(nums, m, right);
        }
        if (nums[left] > nums[m]) {
            return findIndex(nums, left, m);
        }
        return -1;
    }
    int mybsearch(vector<int>& nums, int target, int left, int right) {
        if (left > right) {
            return -1;
        }
        int m = left + (right - left) / 2;
        int m_val = nums[m];
        if (m_val == target) {
            return m;
        }
        if (target < m_val) {
            return mybsearch(nums, target, left, m - 1);
        }
        return mybsearch(nums, target, m + 1, right);
    }
    int search(vector<int>& nums, int target) {
        int len = nums.size();
        if (len == 0) {
            return -1;
        }
        if (len == 1) {
            return nums[0] == target ? 0 : -1;
        }
        int point = findIndex(nums, 0, len - 1);
        if (point == -1) {
            return mybsearch(nums, target, 0, len - 1);
        }
        int result = mybsearch(nums, target, 0, point);
        return result != -1 ? result : mybsearch(nums, target, point + 1, len - 1);
    }
};

int main() {
    vector<int> input{4,5,6,7,0,1,2};
    vector<int>::iterator it = input.begin();
    Solution s{};
    auto result = s.search(input, 0);
    cout << result << endl;
}
