#include <bits/stdc++.h>
#include <cstdio>
using namespace std;
class Solution {
  public:
    // assume current is valid
    string::reverse_iterator rnext_valid(string& S, string::reverse_iterator it, int jmp) {
      if (it == S.rend()) {
        return it;
      }
      auto next = it + 1;    
      if (*next == '#') {
        return rnext_valid(S, next, ++jmp);
      }
      if (jmp) {
        return rnext_valid(S, next, --jmp);
      }
      // printf("next valid = %c\n", *next);
      return next;
    };
    bool backspaceCompare(string& S, string& T) {
      return backspaceCompare(S, T, S.rbegin() - 1, T.rbegin()- 1);
    }
    bool backspaceCompare(string& S, string& T,
        string::reverse_iterator s_it,
        string::reverse_iterator t_it) {
      if (s_it == S.rend() && t_it == T.rend())
        return true;
      if (s_it == S.rend() || t_it == T.rend() || *s_it != *t_it)
        return false;
      return backspaceCompare(S, T, rnext_valid(S, s_it, 0), rnext_valid(T, t_it, 0));
    }
};

int main() {
  string a = "ab##";
  string b = "c#d##";
  Solution s;
  auto result = s.backspaceCompare(a, b);
  cout << result << endl;
}
