#define DEBUG 1

#ifdef DEBUG
#include <bits/stdc++.h>
using namespace std;
#endif

class Solution {
    public:
    int i{0};
    void DFS(int dest_dep, int cur_dep, char parent, char current) {
        if (current < parent)
            return;
        if (cur_dep == dest_dep) {
            ++i;
        } else {
            int i = current;
            while (i <= 5) {
                DFS(dest_dep, cur_dep +1, current, i++);
            }
        }
    }
    int countVowelStrings(int n) {
        // 칸막이는 4개 공은 n개 = 0 x n, 1 1 1 1
        auto sol1 = [n]()->int {
            string str{""};
            for (int i = 0; i < n; ++i) {
                str.push_back('0');
            }
            str += "1111";
            int i = 0;
            do {
                ++i;
            } while (next_permutation(begin(str), end(str)));
            return i;
        };
        DFS(n, 0, 0, 1);
        return i;
    }
};

#ifdef DEBUG
int main() {
    Solution s{};
    int val = s.countVowelStrings(33);
    cout << val << endl;
}
#endif
