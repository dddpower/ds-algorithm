#define DEBUG
#ifdef DEBUG
#include <bits/stdc++.h>
using namespace std;
class Solution {
public:
    bool compare2left(vector<int>& nums, int x) {
        return nums[x - 1] == nums[x];
    }
    bool compare2right(vector<int>& nums, int x) {
        return nums[x] == nums[x + 1];
    }
    // int mySearch_boundary() {
    //     if (l == 0) {
    //         if (nums[l] != nums[l + 1])
    //             return nums[l];
    //     }
    //     if (r == nums.size() - 1) {
    //         if (nums[r] != nums[r - 1])
    //             return nums[r];
    //     }
    // }
    
    int mySearch(vector<int>& nums, int l, int r) {
        assert(l <= r);
        int len = r - l + 1;
        if (l == 0) {
            if (nums[l] != nums[l + 1])
                return nums[l];
        } else if (nums[l - 1] != nums[l] && nums[l] != nums[l + 1]) {
            return nums[l];
        }
        
        if (r == nums.size() - 1) {
            if (nums[r] != nums[r - 1])
                return nums[r];
        } else if (nums[r - 1] != nums[r] && nums[r] != nums[r + 1]) {
            return nums[r];
        }
        if (l == 0 || r == nums.size() - 1) {
            if (l != 0) {

            } else if (r != nums.size() - 1) {

            } else {

            }
        } else if (nums[l] == nums[l - 1] && nums[r] == nums[r + 1]) {
            if (len % 2 == 1) {

            }
        } else if (nums[l] == nums[l + 1] && nums[r - 1] == nums[r]) {
            if (len % 2 == 1) {

            }
        } else {
            if (len % 2 == 0) {

            }
        }
    }
    int singleNonDuplicate(vector<int>& nums) {
    }
};
#endif
#ifdef DEBUG
int main() {

}
#endif
