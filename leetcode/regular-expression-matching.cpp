#define DEBUG

#ifdef DEBUG
#include <bits/stdc++.h>
using namespace std;
#endif

class Solution {
    public:
        // make star expression from post to infix
        string fix(string p) {
            for (auto it = begin(p); it != end(p); ++it) {
                if (*it == '*') 
                    swap(*(it - 1), *it);
            }
            return p;
        }
        // current p is '*'
        bool match_star(string::const_iterator s, decltype(s) p) {
            if (*s == 0)
                return match(s, p + 2);

            if (*(p + 1) == '.' || *(p + 1) == *s) {
                return match_star(s + 1, p) || match(s, p + 2);
            } else {
                return match(s, p + 2);
            }
        }
        bool match(string::const_iterator s, decltype(s) p) {
            if (*s == 0) {
                if (*p == 0)
                    return true;
                if (*p == '*')
                    return match(s, p + 2);
                return false;
            } else {
                if (*p == '*') {
                    return match_star(s, p);
                } else if (*p == '.' || *p == *s) {
                    return match(s + 1, p + 1);
                } else {
                    return false;
                }
            }
        }
        bool isMatch(string s, string p) {
            return match(begin(s), begin(fix(p)));
        }
};

#ifdef DEBUG
int main() {
    Solution s{};
    cout << s.isMatch("ab", ".*c") << endl;
}
#endif
