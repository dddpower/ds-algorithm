#include <bits/stdc++.h>
using namespace std;
class Solution {
    public:
        int numFactoredBinaryTrees(vector<int>& arr) {
            size_t mod = 1e9 + 7;
            map<size_t, size_t> m{};
            for (auto const& val : arr) {
                m.emplace(make_pair(val, 1));
            }
            for (auto i = begin(m); i != end(m); ++i) {
                for (auto j = begin(m); j != m.upper_bound(i->first); ++j) {
                    auto a = i->first;
                    auto b = j->first;
                    auto c = (a * b);
                    if (m.find(c) != end(m)) {
                        m.at(c) += (((a != b) + 1) * m.at(a) * m.at(b)) % mod;
                        m.at(c) %= mod;
                    }
                }
            }
            size_t count = 0;
            for (auto const& p: m) {
                count += p.second;
                count %= mod;
            }
            return count;
        }
};
int main() {
    Solution s{};
    vector<int>v{2,4,8};
    s.numFactoredBinaryTrees(v);
}
