#define DEBUG
#ifdef DEBUG
#include <bits/stdc++.h>
using namespace std;
#endif

class Solution {
    public:
        int majorityElement(vector<int>& nums) {
            int major_num = nums.size() / 2;
            unordered_map<int, int> m;
            for (int num : nums) {
                if(++m[num] > major_num)
                    return num;
            }
            assert("should not reach this point");
            return -1;
        }
};

#ifdef DEBUG
int main() {

}
#endif
