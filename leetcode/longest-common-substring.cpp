#include <bits/stdc++.h>
// #define DEBUG 
using namespace std;
class Solution {
public:
    int longestCommonSubsequence(const std::string& text1, 
                                 const std::string& text2) {
        const int len1 = text1.size();
        const int len2 = text2.size();
        if (!len1 || !len2)
            return 0;
        vector<vector<int>> mem{};
        mem.resize(len1);
        mem.shrink_to_fit();
        for (vector<int>& each_row : mem) {
            each_row.resize(len2);
            each_row.shrink_to_fit();
        }
        for (int i = 0; i < len1; ++i) {
            for (int j = 0; j < len2; ++j) {
                int comp_1 = j == 0 ? 0 : mem[i][j - 1]; 
                int comp_2 = i == 0 ? 0 : mem[i - 1][j];
                int comp_3 = (text1[i] == text2[j]) + 
                    (i == 0 || j == 0 ? 0 : mem[i - 1][j - 1]);
                mem[i][j] = max(comp_1, max(comp_2, comp_3));
            }
        }
#ifdef DEBUG
        cout << "text1 = " << text1 << endl;
        cout << "text2 = " << text2 << endl;
        cout << "memory status" << endl;
        for (auto row : mem) {
            for (auto col : row) {
                cout << col << " ";
            }
            cout << endl;
        }
        cout << endl;
#endif
        return *mem.rbegin()->rbegin();
	}
};
int main() {
    string text1 = "bsbininm";
    string text2 = "jmjkbkjkv";
    Solution s;
    int result = s.longestCommonSubsequence(text1, text2);
    cout << result << endl;
}
