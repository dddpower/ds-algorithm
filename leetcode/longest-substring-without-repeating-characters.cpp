#include <bits/stdc++.h>
using namespace std;
class Solution {
  public:
    int lengthOfLongestSubstring(string s) {
      unordered_map<char, size_t> um{};
      size_t i{0};
      size_t record{0};
      size_t len{0};
      while (i < s.size()) {
        auto result = um.find(s[i]);
        if (result == end(um)) {
          um.insert(make_pair(s[i], i));
          ++i;
          ++len;
        } else {
          record = max(record, len);
          i = result->second + 1;
          len = 0;
          um.clear();
        }
      }
      return max(record, len);
    }
};

int main() {
  string s = "aa";
  Solution sol{};
  unordered_map<char, size_t> um{};
  auto result{sol.lengthOfLongestSubstring(s)};
  cout << result << endl;
}
