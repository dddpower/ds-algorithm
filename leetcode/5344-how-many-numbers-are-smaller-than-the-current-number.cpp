#include <bits/stdc++.h>
using namespace std;
class Solution {
  public:
    vector<int> smallerNumbersThanCurrent(vector<int>& nums) {
      map<int, int> m0, m1, m2;
      vector<pair<int, int>> v;
      int length = nums.size();
      for (int i = 0; i < length; ++i) {
        ++m0[nums[i]];
        m1[i] = nums[i];
      }
      for (auto iter = m0.begin(); iter != m0.end(); ++iter) {
        v.push_back(make_pair(iter->first, iter->second));
      }
      sort(v.begin(), v.end());
      int acc = 0;
      for (auto iter = v.begin(); iter != v.end(); ++iter) {
        m2[iter->first] = acc;
        acc += iter->second;
      }
      vector<int> result;
      for (int i = 0; i < length; ++i) {
        result.push_back(m2[m1[i]]);
      }
      return result;
    }
};

int main() {
  auto test_input = vector<int>{8,1,2,2,3};
  Solution s;
  auto result = s.smallerNumbersThanCurrent(test_input);
  for (auto i = result.begin(); i !=result.end(); ++i) {
    cout << *i << " ";
  }
  cout << endl;
}
