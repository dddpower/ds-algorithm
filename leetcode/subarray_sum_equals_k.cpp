#include <bits/stdc++.h>
using namespace std;
class Solution {
public:
    int calculate(vector<int>const& nums, int len, int k, int b, int e, int sum, int count) {
        while (e < len - 1 && sum < k) {
            sum += nums[++e];
        }
        while (b < e && sum > k) {
            sum -= nums[b++];
        }
        if (sum == k) {
            ++count;
            if (e == len - 1)
                return count;
            sum += nums[++e];
            sum -= nums[b++];
            return calculate(nums, len, k, b, e, sum, count);
        }
        return count;
    }
    int subarraySum(vector<int>const & nums, int k) {
        int len = nums.size();
        if (!len)
            return 0;
        return calculate(nums, len, k, 0, 0, nums[0], 0);
    }
};
