#define DEBUG

#ifdef DEBUG
#include <bits/stdc++.h>
using namespace std;
#endif
class Solution {
    public:
        string minWindow(string S, string T) {
            // if success, returns the iterator i which satiesfies *i = last ch of T.
            // if fail, returns end(S)
            auto forward_parse = [&](string::iterator from)->decltype(from) {
                auto t = begin(T);
                while (t != end(T) && from != end(S)) {
                    if (*from == *t) {
                        ++t;
                        if (t == end(T))
                            break;
                    }
                    ++from;
                }
                return from;
            };
            // Assume forward_parse success.
            // returns the last iterator i, which satisfies *i = first ch of T.
            auto backward_parse = [&](string::iterator rfrom)->decltype(rfrom) {
                // cout << "in backward_parse, " << endl;
                auto rt = rbegin(T);
                while (rt != rend(T)) {
                    // cout << *rfrom << endl;
                    if (*rfrom == *rt) {
                        ++rt;
                    }
                    --rfrom;
                }
                // cout << "end of backward" << endl;
                return rfrom + 1;
            };
            //if fail, returns end(S).
            auto get_begin = [&](string::iterator it)->decltype(it) {
                while (it != end(S)) {
                    if (*it == *begin(T))
                        return it;
                    ++it;
                }
                return it;
            };
            size_t min_val = SIZE_MAX;
            auto ans_b{end(S)};
            auto ans_e{ans_b};
            auto b = begin(S);
            while (b != end(S)) {
                b = get_begin(b);
                if (b == end(S))
                    break;
                auto e = forward_parse(b);
                // cout << "after forward, e = " << *e << endl;
                if (e == end(S))
                    break;
                b = backward_parse(e);
                // cout << "after backward, b = " << *b << endl;
                auto dist = distance(b, e);
                if (min_val > dist) {
                    // cout << "min val update : dist = " << dist <<endl;
                    min_val = dist;
                    ans_b = b;
                    ans_e = e;
                }
                ++b;
            }
            if (min_val == SIZE_MAX)
                return "";
            return string(ans_b, ans_e + 1);
        }
};
#ifdef DEBUG
int main() {
    Solution s{};
    string a = "abccdebdde";
    string b = "bde";
    cout << s.minWindow(a, b) << endl;
}
#endif
