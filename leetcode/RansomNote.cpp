#define DEBUG
#ifdef DEBUG
#include <bits/stdc++.h>
using namespace std;
#endif
class Solution {
    public:
        bool canConstruct(string ransomNote, string magazine) {
            map<char, int> dic{};
            for (char ch : magazine) {
                dic[ch] += 1;
            }
            for (char ch : ransomNote) {
                if (!dic[ch])
                    return false;
                --dic[ch];
            }
            return true;
        }
};

#ifdef DEBUG
int main() {

}
#endif
