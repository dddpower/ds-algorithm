#define DEBUG
#ifdef DEBUG
#include <bits/stdc++.h>
using namespace std;
class Solution {
public:
    int helper(string word1, string word2, int i1, int i2,
            vector<vector<int>>& memory) {
        if (i1 < 0) {
            return i2 + 1;
        }
        if (i2 < 0) {
            return i1 + 1;
        }
        if (!memory[i1][i2]) {
            int val1 = helper(word1, word2, i1 - 1, i2 - 1, memory) + 
                (word1[i1] != word2[i2]);
            int val2 = helper(word1, word2,i1 -1 , i2, memory) + 1;
            int val3 = helper(word1, word2, i1, i2 - 1, memory) + 1;
            memory[i1][i2] = min(min(val1, val2), val3);
        }
        return memory[i1][i2];
    }
    int minDistance(string word1, string word2) {
        vector<vector<int>> m{};
        m.resize(word1.size());
        m.shrink_to_fit();
        for (auto& col : m) {
            col.resize(word2.size());
            col.shrink_to_fit();
        }
        int result = helper(word1, word2, word1.size() - 1, word2.size() - 1, m);
        // print memory for debug
#ifdef DEBUG
        // quest
        for (const auto& col : m) {
            for_each(col.begin(), col.end(), [](auto x) {cout << x << " ";});
            cout << endl;
        }
#endif
        return result;
    }
};
#endif
#ifdef DEBUG
int main() {
    Solution s;
    auto s1 = "horse";
    auto s2 = "ros";
    cout << s.minDistance(s1, s2);
}
#endif
