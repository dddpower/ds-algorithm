#include <string>
#include <algorithm>
#include <cassert>
#include <stdexcept>
#include <iostream>
using namespace std;
class Solution {
	public:
		int reverse(int x) {
			auto temp_str = to_string(x);
			auto first = begin(temp_str);
			if (*first == '-') {
				++first;
			}
			std::reverse(first, end(temp_str));
			auto answer = stoll(temp_str);
			if (answer > INT32_MAX || answer < INT32_MIN)
				return 0;
			return answer;
			// try {
			// 	return stoi(temp_str);
			// } catch (const std::out_of_range& e) {
			// 	return 0;
			// }
		}
};

int main() {
	Solution s{};
	auto answer = s.reverse(-2147483648);
	std::cout << answer << std::endl;
}