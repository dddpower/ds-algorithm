#include <bits/stdc++.h>
using namespace std;
class Solution {
  public:
    int findMaxLength(vector<int>& nums) {
      stack<int> s;
      int max{0};
      int reserved_max;
      for (int num : nums) {
        if (s.empty()) {
          max = max > reserved_max ? max : reserved_max;
          reserved_max = 0;
        }
        if (s.empty() || s.top() == num) {
          s.push(num);
        } else {
          s.pop();
          reserved_max += 2;
        }
      }
      return max;
    }
};
