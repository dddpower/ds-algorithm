#define DEBUG
#ifdef DEBUG
#include <bits/stdc++.h>
using namespace std;
#endif
class Solution {
public:
    // Let's use half open range
    int searchInsert(vector<int>& nums, int target) {
        auto l = begin(nums);
        auto r = end(nums);
        while (l + 1 != r)  {
            auto m = l + (r - l) / 2;
            if (binary_search(m, r, target)) {
                l = m;
            } else {
                r = m;
            }
        }
        return distance(begin(nums), l);
    }

    // int searchInsert(vector<int>& nums, int target) {
    //     return searchInsert(nums, target, 0, nums.size() - 1);
    // }
};
#ifdef DEBUG
int main() {
    vector<int> in{1, 3, 5, 6};
    Solution s{};
    cout << s.searchInsert(in, 5) << endl;
    cout << s.searchInsert(in, 2) << endl;
    cout << s.searchInsert(in, 7) << endl;
    cout << s.searchInsert(in, 0) << endl;
}
#endif
