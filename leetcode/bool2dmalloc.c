#include <stdbool.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
int main() {
  bool** mem;
  int len = 10;
  mem = malloc(len);
  for (int i = 0; i < len; ++i) {
    mem[i] = malloc(sizeof(bool) * len);
    memset(&mem[i], 0, len);
  }

  for (int i = 0; i < len; ++i) {
    for (int j = 0; j < len; ++j) {
      printf("%d\t", mem[i][j]);
    }
    printf("\n");
  }
}
