#include <bits/stdc++.h>
using namespace std;

class Solution {
    public:
        bool jmp(const vector<int>& in, vector<bool>& visit,
                int fin, int current_index) {
            if (visit[current_index])
                return false;
            visit[current_index] = true;
            const int& val = in[current_index];
            if (current_index == fin) {
                return true;
            }
            if (!val)
                return false;
            if (current_index + val >= fin)
                return true;
            bool result = false;
            for (int i = 1; i <= val; ++i) {
                int next_val = current_index + i;
                bool result = jmp(in, visit, fin, current_index + i);
                if (result)
                    return true;
            }
            return false;
        }
        bool canJump(vector<int>& nums) {
            vector<bool> visit{};
            visit.resize(nums.size());   
            visit.shrink_to_fit();
            return jmp(nums, visit, nums.size() - 1, 0);
        }
};


int main() {
    vector<int> test1 = {2, 3, 1, 1, 4};
    vector<int> test2 = {3, 2, 1, 0, 4};
    Solution s;
    cout << s.canJump(test1) << endl;
    cout << s.canJump(test2) << endl;
}
