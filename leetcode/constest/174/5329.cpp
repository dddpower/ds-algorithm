#include <bits/stdc++.h>
using namespace std;
vector<int> input0 {3,3,3,3,5,5,5,2,2,7};
vector<int> input1 {7,7,7,7,7};
vector<int> input2 {1,9};
vector<int> input3 {1000, 1000, 3, 7};
vector<int> input4 {1,2,3,4,5,6,7,8,9,10};
bool my_compare(pair <int, int> first, pair<int, int> second){
  return first.second > second.second;
}
void print(vector<pair<int, int>>& v) {
  for (auto it = v.begin(); it != v.end(); ++it) {
    cout << "(" << it->first <<", " << it->second << ")\t";  
  }
  cout << endl;
}

int minSetSize(vector<int>& arr) {
  int len = arr.size();
  map<int, int> m;
  for (auto it = arr.begin(); it != arr.end(); ++it) {
    m[*it]++;
  }
  vector<pair<int, int>> result(m.begin(), m.end());
  sort(result.begin(), result.end(), my_compare);
  int sum = 0;
  int c = 0;
  // print(result);
  while (sum < len / 2) {
    sum += result[c++].second;
  }
  return c;
}
int main() {
  cout << minSetSize(input0) << endl;
  cout << minSetSize(input1) << endl;
  cout << minSetSize(input2) << endl;
  cout << minSetSize(input3) << endl;
  cout << minSetSize(input4) << endl;
}
