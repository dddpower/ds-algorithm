#include <bits/stdc++.h>
using namespace std;
bool my_compare(int i, int j) {
  return i > j;
}
class Solution {
  public:
    vector<int> kWeakestRows(vector<vector<int>>& mat, int k) {
      vector<int> v(mat.size(), 0);
      int v_i = 0;
      for (auto i = mat.begin(); i != mat.end(); ++i) {
        int c = 0;
        for (auto j = i->begin(); j != i->end(); ++j) {
          if (*j) {
            ++c;
          }
        }
        v[v_i++] = c;
      }
      sort(v.begin(),v.end(),my_compare);
      vector<int> result(v.begin(), v.begin() + k - 1);
      return result;
    }
};
