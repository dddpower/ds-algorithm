#include <bits/stdc++.h>
using namespace std;

// Definition for a Node.
class Node {
public:
    int val;
    vector<Node*> neighbors;
    
    Node() {
        val = 0;
        neighbors = vector<Node*>();
    }
    
    Node(int _val) {
        val = _val;
        neighbors = vector<Node*>();
    }
    
    Node(int _val, vector<Node*> _neighbors) {
        val = _val;
        neighbors = _neighbors;
    }
};

class Solution {
  private:
    unordered_map<Node*, Node*> visit_table;
  public:
    const Node* cloneGraph(const Node* const node) {
      if (!node) {
        return nullptr;
      }
      Node* n = new Node(node->val);
      visit_table[node] = n;
      for (auto i = node->neighbors.begin(); i != node->neighbors.end(); ++i) {
          Node* cloned_node = nullptr;
          Node* val = visit_table[*i];
          if (!val)
            val = cloneGraph(*i);
          n->neighbors.push_back(val);
      }
      return n;
    }
};
