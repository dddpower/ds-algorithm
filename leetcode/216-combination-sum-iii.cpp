#include <bits/stdc++.h>
using namespace std;


void print_vector(vector<int> vec) {
  cout << '[';
  for (const auto& i : vec) {
    cout << i << ", ";
  }
  cout << ']' << endl;
}
class Solution {
public:
    vector<vector<int>> combinationSum3(int k, int n) {
      set<int> s{1,2,3,4,5,6,7,8,9};
      vector<vector<int>> v{};
      vector<int> vv{};
      solve(s, v, vv, k, n);
      return v;
    }
    void solve(set<int> s, 
        vector<vector<int>>& ans, vector<int>& vec, int k, int n) {
      if (k == 0) {
        if (n == 0) {
          ans.emplace_back(vec);
        }
      } else if (n > 0) {
        for (auto it = begin(s); it != end(s);) {
          auto val = *it;
          vec.emplace_back(val);
          s.erase(it++);
          solve(s, ans, vec, k - 1, n - val);
          vec.pop_back();
        }
      }
    }
};
int main() {
  Solution s{};
  auto answer = s.combinationSum3(4, 1);
  for (auto const& i : answer) {
    print_vector(i);
  }
  cout << endl;
}
