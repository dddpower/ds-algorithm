#define DEBUG
#ifdef DEBUG
#include <bits/stdc++.h>
using namespace std;
 struct TreeNode {
     int val;
     TreeNode *left;
     TreeNode *right;
     TreeNode() : val(0), left(nullptr), right(nullptr) {}
     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
 };
#endif

using node_info = map<int, pair<int, int>>; // node_num, depth, parent_num
class Solution {
    public:
        void dfs(TreeNode* parent, TreeNode* root, node_info& rec, int counter) {
            if (root) {
                rec[root->val] = make_pair(counter, parent->val);
                dfs(root, root->left, rec, counter + 1);
                dfs(root, root->right, rec, counter + 1);
            }
        }
        bool isCousins(TreeNode* root, int x, int y) {
            node_info recorder{};
            dfs(root, root, recorder, 0);
            if (recorder[x].first == recorder[y].first &&
                    recorder[x].second != recorder[y].second)
                return true;
            return false;
        }

};
