#define DEBUG
#ifdef DEBUG
#include <bits/stdc++.h>
using namespace std;
#endif
class Solution {
    public:
    size_t change(size_t amount, const vector<int>& coins) {
        auto s = coins.size();
        if (!amount && !s)
            return 1;
        vector<int> m{};
        m.resize(amount + 1);
        for (auto value : coins) {
            int val = value;
            while (val <= amount) {
                m[val] += 1;
                val += value;
            }
            for (int i = 0; i <= amount; ++i) {
                cout << m[i] << " ";
            }
            cout << endl;
        }
        return m[amount];
    }
};
#ifdef DEBUG
int main() {
    Solution s{};
    vector<int> v = {1, 2, 5};
    cout <<s.change(5, v) << endl;
}
#endif

