#define DEBUG 1

#ifdef DEBUG
#include <bits/stdc++.h>
using namespace std;
#endif

class Solution {
    public:
        void merge(vector<int>& nums1, int m, vector<int>& nums2, int n) {
            vector<int>temp{};
            temp.resize(nums1.size());
            size_t i = 0;
            size_t j = 0;
            size_t k = 0;
            int val{0};
            while (i < m && j < n) {
                if (nums1[i] > nums2[j]) {
                    val = nums2[j++];
                } else {
                    val = nums1[i++];
                }
                temp[k++] = val;
            }
            while (i < m) {
                temp[k++] = nums1[i++];
            }
            while (j < n) {
                temp[k++] = nums2[j++];
            }
            nums1 = move(temp);
        }
};

#ifdef DEBUG
int main() {

}
#endif
