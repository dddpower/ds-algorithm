#include <bits/stdc++.h>
using namespace std;

struct TreeNode {
    int val;
    TreeNode *left;
    TreeNode *right;
    TreeNode() : val(0), left(nullptr), right(nullptr) {}
    TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
    TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
};

class Solution {
    public:
        bool isValidSequence(TreeNode* root, vector<int>& arr, int i = 0) {
            if (!root) {
                return false; 
            }
            auto left = root->left;
            auto right = root->right;
            if (root->val == arr[i]) {
                if (i == arr.size() - 1) {
                    if(!left && !right)
                        return true;
                    return false;
                }
                return isValidSequence(left, arr, i + 1) || 
                    isValidSequence(right, arr, i + 1);
            }
            return false;
        }
};
