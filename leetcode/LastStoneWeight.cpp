#include <bits/stdc++.h>
using namespace std;
int lastStoneWeight(vector<int>& stones) {
  priority_queue<int, vector<int>> p_q{};
  for (int ele : stones) {
    p_q.emplace(ele);
  }
  while (true) {
    if (p_q.empty()) {
      return 0;
    }
    if (p_q.size() == 1) {
      return p_q.top();
    }
    int first = p_q.top();
    p_q.pop();
    int second = p_q.top();
    p_q.pop();
    cout << "crashed " << first << ", " << second << '\n';
    int val = abs(first - second);
    if (val)
      cout << "pushed " << val << '\n';
      p_q.emplace(val);
  }
}

int main() {
  auto x = vector<int>{2, 7, 4, 1, 8, 1};
  int result = lastStoneWeight(x
  cout << "last remains " << result << '\n';
}
