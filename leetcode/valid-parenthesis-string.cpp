#define DEBUG
#ifdef DEBUG
#include <bits/stdc++.h>
using namespace std;
#endif

class Solution {
    public:
        bool checkValidString(string s) {
            int min = 0;
            int max = 0;
            for (char ch : s) {
                if (ch == '(') {
                    ++min;
                    ++max;
                } else if (ch == ')') {
                    if (!max)
                        return false;
                    --max;
                    min -= min > 0;
                } else if (ch == '*') {
                    ++max;
                    min -= min > 0;
                } else {
                    assert("invaid ch");
                }
            }
            return min == 0;
        }
};
#ifdef DEBUG
int main() {
    string input = "(*()";
    auto result = Solution().checkValidString(input);
    cout << result << endl;
}
#endif
