#include <bits/stdc++.h>
using namespace std;

class Solution {
  public:
    // fixed by other : reuse arr rather than using extra space
    bool visit(vector<int>& arr, size_t current) {
      // check out of range
      if (current >= arr.size() || current < 0)
        return false;
      // check previous visit
      auto& val = arr[current];
      if (val == -1)
        return false;
      if (val == 0) {
        return true;
      }
      auto temp = val;
      val = -1;
      
      return visit(arr, current + temp)
        || visit(arr, current - temp);
    }
    bool canReach(vector<int>& arr, int start) {
      //dfs
      return visit(arr, start);
    }
};

int main() {

}
