#define DEBUG 1

#ifdef DEBUG
#include <bits/stdc++.h>
using namespace std;
#endif

class Solution {
public:
int ladderLength(string beginWord, string endWord, vector<string>& wordList) {
    auto is_reachable = [](const string& w1, const string& w2)->bool {
        auto it1 = begin(w1);
        auto it2 = begin(w2);

        bool diffs{false};
        // Length of w1 & w2 are same
        while (it1 != end(w1)) {
            if (*it1 != *it2) {
                if (diffs == false)
                    diffs = true;
                else return false;
            }
            ++it1;
            ++it2;
        }
        return true;
    };
    unordered_map<string, size_t> dist{};
    queue<string> q{};
    q.emplace(beginWord);
    dist[beginWord] = 1;
    while (!q.empty()) {
        auto current = q.front();
        q.pop();
        for (auto& word: wordList) {
            if (dist[word] == 0 && is_reachable(current, word)) {
                q.emplace(word);
                dist[word] = dist[current] + 1;
                if (word == endWord)
                    return dist[word];
            }
        }
    }
    return 0;
}
};

#ifdef DEBUG
int main() {
    Solution s{};
    vector<string> words = {"hot","dot","dog","lot","log","cog"};
    string const b = "hit";
    string const e = "cog";
    auto answer = s.ladderLength(b, e, words);
    cout << answer << endl;
}
#endif
