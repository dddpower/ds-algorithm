#define DEBUG
#ifdef DEBUG
#include <bits/stdc++.h>
using namespace std;
#endif

class Solution
{
public:
    bool isPerfectSquar(int num, int l, int r)
    {
        if (l > r)
            return false;
        int m = l + (r - l) / 2;
        int div = num / m;
        int remain = num % m;
        if (div == m) {
            if (remain)
                return isPerfectSquar(num, m + 1, r);
            return true;
        }
        if (div < m) {
            return isPerfectSquar(num, l, m - 1);
        }
        return isPerfectSquar(num, m + 1, r);
    }
    bool isPerfectSquare(int num)
    {
        if (num == 1)
            return true;
        return isPerfectSquar(num, 2, num / 2);
    }
};

#ifdef DEBUG
int main() {
    Solution s{};
    cout << s.isPerfectSquare(14) << endl;
}
#endif
