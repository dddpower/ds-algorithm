#include <bits/stdc++.h>
using namespace std;
class Solution {
  public:
    bool isNewLand(vector<vector<bool>>& v, vector<vector<char>>& grid,
        int row, int col) {
      return grid[row][col] =='1' && v[row][col] == false;
    }
    void dfs(vector<vector<bool>>& v, vector<vector<char>>& grid,
             int current_row, int current_col) {
      int row = grid.size();
      int col = grid[0].size();
      if (current_row < 0 || current_row == row || current_col < 0
          || current_col == col) {
        return;
      }
      if (isNewLand(v, grid, current_row, current_col)) {
        v[current_row][current_col] = true;
        dfs(v, grid, current_row, current_col + 1);
        dfs(v, grid, current_row + 1, current_col);
        dfs(v, grid, current_row , current_col - 1);
        dfs(v, grid, current_row - 1, current_col);
      }
    }
    int numIslands(vector<vector<char>>& grid) {
      int row = grid.size();
      if (!row)
          return 0;
      int col = grid[0].size();
      vector<vector<bool>> visit{};
      visit.resize(row);
      for (auto& each_row : visit) {
          each_row.resize(col);
      }
      int count = 0;
      for (int i = 0; i < row; ++i) {
        for (int j = 0; j < col; ++j) {
          if (isNewLand(visit, grid, i, j)) {
            ++count;
            visit[i][j] = true;
            dfs(visit, grid, i, j + 1);
            dfs(visit, grid, i, j - 1);
            dfs(visit, grid, i + 1, j);
            dfs(visit, grid, i - 1, j);
          }
        }
      }
      return count;
    }
};