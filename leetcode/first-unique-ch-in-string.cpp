#define DEBUG
#ifdef DEBUG
#include <bits/stdc++.h>
#endif
using namespace std;
class Solution {
    public:
        int firstUniqChar(string s) {
            map<char, pair<int, int>> m;
            int min_val = INT_MAX;
            int index = -1;
            for (char ch : s) {
                ++index;
                auto& cur = m[ch];
                if (!cur.first)
                    cur.second = index;
                ++cur.first;
            }
            unsigned int min = UINT_MAX;
            for (auto el : m) {
                const auto& val = el.second;
                if (val.first == 1 && val.second < min) {
                    min = val.second;
                }
            }
            if (min == UINT_MAX)
                return -1;
            return min;
        }
};

#ifdef DEBUG
int main() {

}
#endif
