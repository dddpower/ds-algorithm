#include <bits/stdc++.h>
using namespace std;
std::ostream& operator<< (std::ostream& out, const vector<int>& v) {
    if ( !v.empty() ) {
        out << '[';
        std::copy(v.begin(), v.end(), std::ostream_iterator<int>(out, ", "));
        out << "\b\b]";
        // use two ANSI backspace characters '\b' to overwrite final ", "
    }
    return out;
}
class Solution {
  public:
    void dfs(vector<vector<int>>& list,
        vector<int>& record, vector<int>& candidates, int index, int end, 
        int target) {
      if (target < 0) {
        return;
      } else if (target == 0) {
        list.emplace_back(record);
      } else {
        while (index <= end) {
          auto val = candidates[index++];
          record.emplace_back(val);
          dfs(list, record, candidates, index - 1, end, target - val);
          record.pop_back();
        }
      }
    }
    vector<vector<int>> combinationSum(vector<int>& candidates, int target) {
      vector<vector<int>> list{};
      vector<int> record{};
      dfs(list, record, candidates, 0, candidates.size() - 1, target);
      return list;
    }
};



int main() {
  Solution s{};
  vector<int> candidates = {2};
  int target = 1;
  auto answer = s.combinationSum(candidates, target);
  for (const auto& v : answer) {
    cout << v << ", ";
  }
  cout << endl;
}
