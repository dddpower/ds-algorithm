#include <bits/stdc++.h>
using namespace std;
class Solution {
  public:
    vector<int> twoSum(vector<int>& nums, int target) {
      auto length = nums.size();
      for (int i = 0; i < length; ++i) {
        for (int j = i; j < length; ++j) {
          if (nums[i] + nums[j] == target) {
            return vector<int>{nums[i], nums[j]};
          }
        }
      }
      return vector<int>{};
    }
};

int main() {
  Solution s{};
  vector<int> test{2, 7, 11, 5};
}
