#include <bits/stdc++.h>
using namespace std;
// Definition for a binary tree node.
struct TreeNode {
    int val;
    TreeNode *left;
    TreeNode *right;
    TreeNode() : val(0), left(nullptr), right(nullptr) {}
    TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
    TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), 
    left(left), right(right) {}
};

class Solution {
    public:
        // we see L + R, and record max while calculating height
        // count_height of single node is 1
        int count_height(TreeNode* root, int& max_diameter) {
            if (!root)
                return 0;
            int l_count = count_height(root->left, max_diameter);
            int r_count = count_height(root->right, max_diameter);
            max_diameter = max(max_diameter, l_count + r_count);
            return max(l_count, r_count) + 1;
        }
        int diameterOfBinaryTree(TreeNode* root) {
            int max_diameter = 0;
            count_height(root, max_diameter);
            return max_diameter;
        }
};
