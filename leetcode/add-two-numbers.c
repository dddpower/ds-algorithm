#include <stdlib.h>
typedef struct _ListNode ListNode;
struct _ListNode {
    int val;
    ListNode* next;
};
ListNode* addTwoNumbers(ListNode* l1, ListNode* l2) {
    if (!l1)
        return l2;
    if (!l2)
        return l1;
    ListNode* head = l1;
    int carry = 0;
    int val = l1->val + l2->val + carry;
    l1->val = val % 10;
    carry = val / 10;
    while (l1->next && l2->next) {
        l1 = l1->next;
        l2 = l2->next;
        val = l1->val + l2->val + carry;
        l1->val = val % 10;
        carry = val / 10;
    }
    if (l1->next) {
        l1 = l1->next;
        val = l1->val + carry;
        l1->val = val % 10;
        carry = val / 10;
    }
    if (l2->next) {
        l1->next = l2->next;
        l1 = l1->next;
        val = l1->val + carry;
        l1->val = val % 10;
        carry = val / 10;
    }
    while (l1->next && carry) {
        l1= l1->next;
        val = l1->val + carry;
        l1->val = val % 10;
        carry = val / 10;
    }
    if (carry) {
        ListNode* last = malloc(sizeof(ListNode));
        last->val = 1;
        last->next = NULL;
        l1->next = last;
    }
    return head;
}
