#include <bits/stdc++.h>
using namespace std;
class Solution {
  public:
    int largestUniqueNumber(vector<int>& A) {
      map<int, int, greater<int>> m{};
      for (auto& el : A) {
        ++m[el];
      }
      for (auto& el : m) {
        if (el.second == 1) {
          return el.first;
        }
      }
      return -1;
    }
};
