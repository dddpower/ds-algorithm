#include <bits/stdc++.h>
using namespace std;

class Solution {
public:
    vector<int> productExceptSelf(vector<int>& nums) {
        vector<int> mem_of_right{};
        mem_of_right.resize(nums.size());
        mem_of_right.shrink_to_fit();
        vector<int> mem_of_left{mem_of_right};
        
        // mem_of_left[x] = product of 0 ~ x
        // mem_of_right[x] = product of [n-1] ~ x
        vector<int>::iterator it = mem_of_left.begin();
        int acc = 1;
        for (auto element : nums) {
            acc *= element;
            *it = acc;
            ++it;
        }
        acc = 1;
        vector<int>::reverse_iterator rit = mem_of_right.rbegin();
        for (auto it = nums.rbegin(); it != nums.rend(); ++it) {
            acc *= *it;
            *rit = acc;
            ++rit;
        }
        nums[0] = mem_of_right[1];
        for (int i = 1; i < nums.size() - 1; ++i) {
            nums[i] = mem_of_right[i + 1] * mem_of_left[i - 1];
        }
        nums[nums.size() - 1] = mem_of_left[nums.size() - 2];
        return nums;
    }
};

int main() {
    Solution s;
    vector<int> intput{1,2,3,4};
    auto x = s.productExceptSelf(intput);
    for_each(x.begin(), x.end(), [](int el) {cout << el << " ";});
}
