#define LEETCODE
#ifdef LEETCODE
#include <bits/stdc++.h>
using namespace std;
#endif
class Solution {
  public:
    void reverseString(vector<char>& s) const {
      reverse(begin(s), end(s));
    }
    void reverseString(vector<char>& s) {
      vector<char>::iterator i = begin(s);
      vector<char>::iterator j = end(s);
      while (distance(i , j) > 0) {
        swap(*i++, *--j);
      }
    }
};
#ifdef LEETCODE
int main() {

}
#endif

