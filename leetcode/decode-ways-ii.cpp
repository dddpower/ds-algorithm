#define LOCAL

#ifdef LOCAL
#include <bits/stdc++.h>
using namespace std;
#endif
class Solution {
  public:
    int numDecodings(string s) {
      size_t MOD = 1e9 + 7;
      auto len = s.size();
      if (!len) {
        return 0;
      } else if (len == 1) {
        if (s[0] == '0') {
          return 0;
        } else if (s[0] =='*') {
          return 9;
        } else {
          return 1;
        }
      } else {// len >= 2
        if (s[0] == '0')
          return 0;
        vector<decltype(len)> dp{};
        dp.resize(len);
        dp.shrink_to_fit();
        // set dp[0]
        if (s[0] == '0') {
          dp[0] = 0;
        } else if (s[0] =='*') {
          dp[0] = 9;
        } else {
          dp[0] = 1;
        }
        // set dp[1]
        if (s[1] == '0') {
          if (s[0] == '*') {
            dp[1] = 2;
          } else if (s[0] == '1' || s[0] == '2') {
            dp[1] = 1;
          } else {
            return 0;
          }
        } else if (s[1] == '*') {
          if (s[0] == '*') {
            dp[1] = dp[0] * 9 + 15;
          } else if (s[0] == '1') {
            dp[1] = dp[0] * 9 + 9;
          } else if (s[0] == '2') {
            dp[1] = dp[0] * 9 + dp[0] * 6;
          } else {
            dp[1] = dp[0] * 9;
          }
        } else if (s[1] <= '6') {
          if (s[0] == '*') {
            dp[1] = dp[0] + 2;
          } else if (s[0] == '1' || s[0] == '2') {
            dp[1] = dp[0] + 1;
          } else {
            dp[1] = dp[0];
          }
        } else {  // 7, 8, 9
          if (s[0] == '*') {
            dp[1] = dp[0] + 1;
          } else if (s[0] == '1') {
            dp[1] = dp[0] + 1;
          } else {
            dp[1] = dp[0];
          }
        }
        // set dp[2~]
        for (size_t i = 2; i < len;++i) {
          if (s[i] == '0') {
            if (s[i - 1] == '*') {
              dp[i] = dp[i - 2] * 2 % MOD;
            } else if (s[i - 1] == '1' || s[i - 1] == '2') {
              dp[i] = dp[i - 2];
            } else {
              return 0;
            }
          } else if (s[i] == '*') {
            if (s[i - 1] == '*') {
              dp[i] = dp[i - 1] * 9 % MOD + dp[i - 2] * 15 % MOD;
            } else if (s[i - 1] == '1') {
              dp[i] = dp[i - 1] * 9 % MOD + dp[i - 2] * 9 % MOD;
            } else if (s[i - 1] == '2') {
              dp[i] = dp[i - 1] * 9 % MOD + dp[i - 2] * 6 % MOD;
            } else {
              dp[i] = dp[i - 1] * 9 % MOD;
            }
          } else if (s[i] <= '6') {
            if (s[i - 1] == '*') {
              dp[i] = dp[i - 1] + dp[i - 2] * 2 % MOD;
            } else if (s[i - 1] == '1' || s[i - 1] == '2') {
              dp[i] = dp[i - 1] + dp[i - 2];
            } else {
              dp[i] = dp[i - 1];
            }
          } else {  // 7, 8, 9
            if (s[i - 1] == '*') {
              dp[i] = dp[i - 1] + dp[i - 2];
            } else if (s[i - 1] == '1') {
              dp[i] = dp[i - 1] + dp[i - 2];
            } else {
              dp[i] = dp[i - 1];
            }
          }

          
          dp[i] %= MOD;
        }
        
        return dp[len - 1];
      }
    }
};

#ifdef LOCAL
int main() {
  Solution s{};
  auto val = s.numDecodings("*********");
  cout << val << endl;
}
#endif
