#include <bits/stdc++.h>
using namespace std;
class Solution {
  public:
    string longestPalindrome(string s) {
      pair<size_t, string> max{0,""};

      // we don't have to look up last character
      for (size_t i = 0; i < s.size(); ++i) {
        // even case
        int a = i;
        auto b = a + 1;
        size_t count = 0;
        while (a >= 0 && b <= s.size() - 1 && s[a] == s[b]) {
          count += 2;
          --a;
          ++b;
        }
        if (count > max.first) {
          max = make_pair(count, s.substr(a + 1, b - a - 1));
        }
        // odd case
        a = i - 1;
        b = i + 1;
        count = 1;
        while (a >= 0 && b <= s.size() - 1 && s[a] == s[b]) {
          count += 2;
          --a;
          ++b;
        }
        if (count > max.first) {
          max = make_pair(count, s.substr(a + 1, b - a - 1));
        }
      }
      return max.second;
    }
};

int main() {
 Solution s{};
 cout << s.longestPalindrome("ac") << endl;
}
