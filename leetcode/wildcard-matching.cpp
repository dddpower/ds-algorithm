#define DEBUG

#ifdef DEBUG
#include <bits/stdc++.h>
using namespace std;
#endif

using vbool = pair<bool, bool>;
using table = vector<vector<vbool>>;
table t{};
string::const_iterator b_s{}, b_p{};
class Solution {
    public:
        vbool get_val(string::const_iterator s, decltype(s) p) {
            return t[distance(b_s, s)][distance(b_p, p)];
        }
        void put_val(string::const_iterator s, decltype(s) p, vbool val) {
            t[distance(b_s, s)][distance(b_p, p)] = val;
        }
        bool match(string::const_iterator s, decltype(s) p) {
            auto t_val = get_val(s, p);
            // if visit
            if (t_val.first) {
                return t_val.second;
            }
            if (*s == 0) {
                if (*p == 0) {
                    // exit condition. don't need to record
                    return true;
                } else if (*p == '*') {
                    return match(s, p + 1);
                } else {
                    put_val(s, p, make_pair(true, false));
                    return false;
                }
            } else {
                if (*p == '?' || *s == *p) {
                    put_val(s, p, make_pair(true, true));
                    return match(s + 1, p + 1);
                } else if (*p == '*') {
                    return match(s + 1, p) || match(s, p + 1);
                } else {
                    put_val(s, p, make_pair(true, false));
                    return false;
                }
            }
            put_val(s, p, make_pair(true, false));
            return false;
        }
        bool isMatch(string s, string p) {
            t.resize(s.size() + 1);
            t.shrink_to_fit();
            for (auto& row : t) {
                row.resize(p.size() + 1);
                row.shrink_to_fit();
            }
            b_s = begin(s);
            b_p = begin(p);
            return match(begin(s), begin(p));           
        }
};

#ifdef DEBUG
int main() {

}
#endif

