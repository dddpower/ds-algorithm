#define DEBUG
#ifdef DEBUG
#include <bits/stdc++.h>
using namespace std;
#endif
class Solution {
    public:
        int maxScore(vector<int>& cardPoints, int k) {
            int sum{0};
            vector<int>::iterator it = cardPoints.begin();
            vector<int>::reverse_iterator rit = cardPoints.rbegin();
            int count = k;
            while (count--)
                sum += *it++;
            count = k;
            int max_mem{sum};
            while (count--) {
                sum = sum - *--it + *rit++;
                max_mem = max(max_mem, sum);
            }
            return max_mem;
        }
};

#ifdef DEBUG
int main() {

}
#endif
