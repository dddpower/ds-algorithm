#define DEBUG
#ifdef DEBUG
#include <bits/stdc++.h>
using namespace std;
class Solution {
    public:
        const int countBitsSingle(int num) {
            int count = 0;
            while (num) {
                num &= num - 1;
                ++count;
            }
            return count;
        }
        vector<int> countBits(int num) {
            vector<int> result{};
            for (int i = 1; i <= num; ++i)
                result.emplace_back(countBitsSingle(i));
            return result;
        }
};
#endif
#ifdef DEBUG
int main() {

}
#endif

