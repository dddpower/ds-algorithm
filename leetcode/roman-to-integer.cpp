#include <bits/stdc++.h>
using namespace std;
class Solution {
  public:
    int romanToInt(string s) {
      size_t sum{0};
      size_t cur{0};
      size_t pre{0};
      for (auto ch: s) {
        switch(ch) {
          case 'I': // 1
            cur = 1;
            break;
          case 'V': // 5
            cur = 5;
            break;
          case 'X': // 10
            cur = 10;
            break;
          case 'L': // 50
            cur = 50;
            break;
          case 'C': // 100
            cur = 100;
            break;
          case 'D': // 500
            cur = 500;
            break;
          case 'M': // 1000
            cur = 1000;
            break;
          default:
            cout << "wrong " << ch << endl;
        }
        sum += cur;
        if (pre < cur) {
          sum -= pre * 2;
        }         
        pre = cur;
      }
      return sum;
    }
};

int main() {

}
