#include <bits/stdc++.h>
using namespace std;
class Solution {
  public:
    vector<vector<int>> threeSum(vector<int>& nums) {

      vector<int> v{};
      v.resize(nums.size());
      fill(begin(v), end(v), 1);
      fill(begin(v), begin(v) + 3, 0);
      vector<vector<int>> result;
      do {
        array<size_t, 3> arr;
        size_t j = 0;
        for(size_t i = 0; i < nums.size(); ++i) {
          if (v[i] == 0) {
            arr[j++] = i;
          }
        }
        if ((nums[arr[0]] != nums[arr[1]] && nums[arr[1]] != nums[arr[2]] && nums[arr[2]] != 
              nums[arr[0]]) &&
            nums[arr[0]] + nums[arr[1]] + nums[arr[2]] == 0) {
          vector<int> x{nums[arr[0]], nums[arr[1]], nums[arr[2]]};
          result.emplace_back(x);
        }
      } while(next_permutation(begin(v), end(v)));
      return result;
    }
};
int main() {
  auto print_vector = [](const vector<int>& vec) -> void {
    for_each(begin(vec), end(vec), [](int n){cout << n << " ";});
    cout << endl;
  };
  Solution s{};
  vector<int> vec{-1,0,1,2,-1,-4};
  auto ans = s.threeSum(vec);
  for (auto& vec : ans) {
    print_vector(vec);
  }
}
