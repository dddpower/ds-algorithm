#define LEETCODE
#ifdef LEETCODE
#include <bits/stdc++.h>
using namespace std;
#endif
class Solution {
  public:
    struct {
      bool operator()(vector<int> a, decltype(a) b) const {
        if (a[0] == b[0])
          return a[1] < b[1];
        return a[0] > b[0];
      }
    } customComp;
    void printPeople(vector<vector<int>>& people) {
      for (auto p : people) {
        printf("[%d, %d], ", p[0], p[1]);
      }
      cout << endl;
    }
    vector<vector<int>> reconstructQueue(vector<vector<int>>& people) {
      sort(begin(people), end(people), customComp);
      for (auto i = begin(people); i < end(people); ++i) {
        auto temp = i;
        while (distance(begin(people), i) > (*i)[1]) {
          swap(*(i - 1), *i);
          --i;
        }
        i = temp;
      }
      return people;
    }
};
#ifdef LEETCODE
int main() {
  vector<vector<int>> input {{7, 0}, {4, 4}, {7, 1}, {5, 0}, {6, 1}, {5, 2}};
  Solution s;
  auto result = s.reconstructQueue(input);
}
#endif
