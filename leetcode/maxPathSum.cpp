#include <bits/stdc++.h>
using namespace std;
// Definition for a binary tree node.
struct TreeNode {
    int val;
    TreeNode *left;
    TreeNode *right;
    TreeNode() : val(0), left(nullptr), right(nullptr) {}
    TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
    TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), 
    left(left), right(right) {}
};

class Solution {
    public:
        int max_path(TreeNode* root, int& mem_max) {
            if (!root) {
                return INT_MIN;
            }
            int root_val = root->val;
            int l = max(max_path(root->left, mem_max), 0);
            int r = max(max_path(root->right, mem_max), 0);
            int new_price = root_val + l + r;
            mem_max = max(mem_max, new_price);
            return root_val + max(l, r);
        }
        int maxPathSum(TreeNode* root) {
            int mem_max = INT_MIN;
            max_path(root, mem_max);
            return mem_max;
        }
};
