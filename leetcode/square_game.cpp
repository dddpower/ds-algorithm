#include <bits/stdc++.h>
using namespace std;

int game(vector<vector<bool>>& input) {
    const int&& row_len = input.size();
    if (!row_len)
        return 0;
    const int&& col_len = input.begin()->size();
    vector<vector<int>> mem{};
    mem.resize(row_len);
    mem.shrink_to_fit();
    for (auto& vec_int : mem) {
        vec_int.resize(col_len);
        vec_int.shrink_to_fit();
    }
    for (int i = 0; i < row_len; ++i) {
        mem[i][0] = static_cast<int>(input[i][0]);
    }
    for (int i = 0; i < col_len; ++i) {
        mem[0][i] = static_cast<int>(input[0][i]);
    }
    return 0;
}
int main() {
    
}
