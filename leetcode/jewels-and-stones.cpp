#define DEBUG
#ifdef DEBUG
#include <bits/stdc++.h>
using namespace std;
#endif
class Solution {
    public:
        int numJewelsInStones(string J, string S) {
            map<char, bool> book;
            for (char ch : J) {
                book[ch] = true;
            }
            int count = 0;
            for (char ch : S) {
                if (book[ch])
                    ++count;
            }
            return count;
        }
};
#ifdef DEBUG
int main() {

}
#endif
