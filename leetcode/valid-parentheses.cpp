#define DEBUG 1

#ifdef DEBUG
#include <bits/stdc++.h>
using namespace std;
#endif
class Solution {
public:
  bool isValid(string s) {
    string st{""};
    auto isClose = [](char ch)->bool {
        return ch == ')' || ch == ']' || ch == '}';
    };
    auto isOpen = [](char ch)->bool {
        return ch == '(' || ch == '[' || ch == '{';
    };
    for (auto it: s) {
        if (isClose(it)) {
            if (st.empty())
                return false;
            auto top = st.back();
            if (it == ')') {
                if (top != '(')
                    return false;
            } else if (it == '}') {
                if (top != '{')
                    return false;
            } else if (it == ']') {
                if (top != '[')
                    return false;
            }
            st.pop_back();
        } else {
            st.push_back(it);
        }
    }
    if (st.empty())
        return true;
    return false;
  }
};

#ifdef DEBUG
int main() {}
#endif
