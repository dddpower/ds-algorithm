#include <bits/stdc++.h>
using namespace std;

// Definition for a binary tree node.
struct TreeNode {
    int val;
    TreeNode *left;
    TreeNode *right;
    TreeNode(int x) : val(x), left(NULL), right(NULL) {}
};

class Solution {
public:
    bool AddNode(TreeNode* root, int num) {
        int root_val = root->val;
        assert(root_val != num);
        if (num < root_val) {
            if (root->left) {
                return AddNode(root->left, num);
            }
            root->left = new TreeNode(num);
            return true;
        }
        if (root_val < num) {
            if (root->right) {
                return AddNode(root->right, num);
            }
            root->right = new TreeNode(num);
            return true;
        }
        return false;
    }
    TreeNode* bstFromPreorder(vector<int>& preorder) {
        if (preorder.size() == 0) {
            return nullptr;
        }
        TreeNode* origin = new TreeNode(*preorder.begin());
        for (auto it = preorder.begin() + 1; it != preorder.end(); ++it) {
            AddNode(origin, *it);
        }
        return origin;
    }
};

int main() {

}
