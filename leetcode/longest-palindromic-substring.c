#include <stdio.h>
#include <stdbool.h>
#include <string.h>
enum {
    MAX_LENGTH = 1000
};

bool mem[MAX_LENGTH][MAX_LENGTH];
void print_mem(int len) {
  printf("memory result\n");
  for (int i = 0; i < len; ++i) {
    for (int j = 0; j < len; ++j)
      printf("%d\t", mem[i][j]);
    printf("\n");
  }
  printf("\n");
}
bool my_compare(const void* a, const void* b) {
  return *(char*)a == *(char*)b;
}
void update_table(char* s, int start_row, int start_col, int len,
    bool(*compare)(const void *a, const void *b)) {
  int i = start_row;
  int j = start_col;
  while (i >= 0 && j < len && compare(&s[i], &s[j])) {
    mem[i][j] = true;
    --i;
    ++j;
  }
}
char* longestPalindrome(char * s) {
    int len = strlen(s);
    int max = 0;
    int mem_i = 0, mem_j = -1;
    int c;
    for (c = 0; c < len; ++c) {
        update_table(s, c, c, len, my_compare);
        update_table(s, c, c + 1, len, my_compare);
    }
    for (int i = 0; i < len; ++i) {
      for (int j = i; j < len; ++j) {
        if (mem[i][j]) {
          int val = j - i + 1;
          if (max < val) {
            max = val;
            mem_i = i;
            mem_j = j;
          }
        }
      }
    }
    char* result = &s[mem_i];
    s[mem_j + 1] = 0;
    // print_mem(len);
    return result;
}

int main() {
  char s[MAX_LENGTH + 1];
  scanf("%s", s);
  char * result = longestPalindrome(s);
  printf("%s\n", result);
}
