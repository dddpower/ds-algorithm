#include <bits/stdc++.h>
using namespace std;

class Solution {
public:
    bool halvesAreAlike(string s) {
      auto is_vowels = [](char ch) -> bool {
        if (ch == 'a' || ch == 'e' || ch == 'i' || ch == 'o' || ch == 'u' ||
            ch == 'A' || ch == 'E' || ch == 'I' || ch == 'O' || ch == 'U')
          return true;
        return false;
      };
      size_t i{0};
      size_t count{0};
      for (; i < s.size() / 2; ++i) {
        if (is_vowels(s[i]))
          ++count;
      }
      for (; i < s.size(); ++i) {
        if (is_vowels(s[i])) {
          --count;
        }
      }
      return !count;
    }
};

int main() {
  Solution s;
  cout << s.halvesAreAlike("book") << endl;
  cout << s.halvesAreAlike("textbook") << endl;
}
