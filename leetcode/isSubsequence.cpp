#define DEBUG
#ifdef DEBUG
#include <bits/stdc++.h>
using namespace std;
#endif
class Solution {
public:
    bool isSubsequence(string s, string t) {
        if (s.empty())
            return true;
        auto i = begin(s);
        auto j = begin(t);
        while (j != end(t)) {
            if (*i == *j) {
                ++i;
                if (i == end(s))
                    return true;
            }
            ++j;
        }
        return false;
    }
};
#ifdef DEBUG
int main() {

}
#endif


