#include <bits/stdc++.h>
using namespace std;
class Solution {
  using direction = bool;
  int calc(string& s, int start_index, direction dir, int dist) {
    if (dir)
      dist *= -1;
    int val = start_index + dist;
    if (val < 0) 
      val += s.length();
    val %= s.length();
    cout << val << endl;
    return val;
  }
  string reconstruct(string& s, int begin) {
    return s.substr(begin) + s.substr(0, begin);
  }
  public:
    string stringShift(string& s, vector<vector<int>>& shift) {
      int begin = 0;
      for (auto single_op : shift) {
        begin = calc(s, begin, single_op[0], single_op[1]);
      }
      cout << "final begin : " << begin << endl;
      return reconstruct(s, begin);
    }
};
int main() {
  Solution s;
  string str{"abc"};
  vector<int> ins1 = {0, 1};
  vector<int> ins2 = {1, 2};
  vector<vector<int>> vec = {ins1, ins2};
  cout << s.stringShift(str, vec) << endl;
}
