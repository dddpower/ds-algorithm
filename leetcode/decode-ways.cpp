#define LOCAL

#ifdef LOCAL
#include <bits/stdc++.h>
using namespace std;
#endif
class Solution {
  public:
    int numDecodings(string s) {
      auto len = s.size();
      if (!len || s[0] == '0') {
        return 0;
      } else if (len == 1) {
        return 1;
      } else {
        vector<decltype(len)> dp{};
        dp.resize(len);
        dp.shrink_to_fit();
        dp[0] = 1;
        auto cur_ch = s[1];
        auto pre_ch = s[0];
        if (cur_ch == '0') {
          if (pre_ch == '1' || pre_ch == '2') {
            dp[1] = 1;
          } else {
            return 0;
          }
        } else if (cur_ch <= '6') {
          if (pre_ch == '1' || pre_ch == '2') {
            dp[1] = 2;
          } else {
            dp[1] = 1;
          }
        } else {
          if (pre_ch == '1') {
            dp[1] = 2;
          } else {
            dp[1] = 1;
          }
        }
        for (size_t i = 2; i < len; ++i) {
          auto cur_ch = s[i];
          auto pre_ch = s[i - 1];
          if (cur_ch == '0') {
            if (pre_ch == '1' || pre_ch == '2') {
              dp[i] = dp[i - 2];
            } else {
              return 0;
            }
          } else if (cur_ch <= '6') {
            if (pre_ch == '0') {
              dp[i] = dp[i - 1];
            } else if (pre_ch <= '2') {
              dp[i] = dp[i - 1] + dp[i - 2];
            } else {
              dp[i] = dp[i - 1];
            }
          } else {
            if (pre_ch == '1') {
              dp[i] = dp[i - 1] + dp[i - 2];
            } else {
              dp[i] = dp[i - 1];
            }
          }
        }
        return dp[len - 1];
      }
    }
};

#ifdef LOCAL
int main() {

}
#endif
