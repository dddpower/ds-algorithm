#include <bits/stdc++.h>
using namespace std;

class Solution {
    public:
        bool canJump(vector<int>& nums) {
            int end = nums.size() - 1;
            if (end < 0) {
                return false;
            }
            int farest = 0;
            for (int i = 0; i <= end; ++i) {
                if (i > farest) {
                    return false;
                }
                if (i + nums[i] > farest) {
                    farest = i + nums[i];
                }
            }
            return nums[end];
        }
};
int main() {

}
