#define DEBUG
#ifdef DEBUG
#include <bits/stdc++.h>
using namespace std;
#endif
template<typename T>
void print_vector(vector<T> v);
class Solution {
    public:
    int twoCitySchedCost(vector<vector<int>>& costs) {
        struct {
            using el_type = decltype(*costs.begin());
            bool operator() (el_type a, el_type b) const {
                return abs(a[0] - a[1]) < abs(b[0] - b[1]);
            }
        } customLess;
        sort(begin(costs), end(costs), customLess);
        int min_sum{0};
        int a_remain = costs.size() / 2;
        int b_remain = a_remain;
        auto it = begin(costs);
        while (a_remain && b_remain ) {
            if ((*it)[0] < (*it)[1]) {
                --a_remain;
                min_sum += (*it)[0];
            } else {
                min_sum += (*it)[1];
                --b_remain;
            }
            ++it;
        }
        while (a_remain) {
            min_sum += (*it)[0];
            ++it;
            --a_remain;
        }
        while (b_remain) {
            min_sum += (*it)[1];
            ++it;
            --b_remain;
        }
        return min_sum;
    }
};
#ifdef DEBUG
template<typename T>
void print_vector(vector<T> v) {
    for (auto e : v) {
        cout << e << " ";
    }
    cout << endl;
}
int main() {
    vector<vector<int>> in = {{10,20},{30,200},{400,50},{30,20}};
    Solution s;
    int result = s.twoCitySchedCost(in);
}
#endif
