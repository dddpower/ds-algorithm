#include <bits/stdc++.h>
using namespace std;

class Solution {
    public:
        //about visit : 0 not visit, 1 reachable, -1 unreachable
        bool jmp(const vector<int>& in, vector<int>& visit,
                int fin, int current_index) {
            if (visit[current_index] == 1)
                return false;
            visit[current_index] = true;
            const int& val = in[current_index];
            if (current_index == fin) {
                return true;
            }
            if (!val) {
                return false;
            }
            if (current_index + val >= fin)
                return true;
            bool result = false;
            for (int i = 1; i <= val; ++i) {
                int next_val = current_index + i;
                bool result = jmp(in, visit, fin, current_index + i);
                if (result)
                    return true;
            }
            return false;
        }
        bool canJump(vector<int>& nums) {
            vector<int> visit{};
            visit.resize(nums.size());   
            visit.shrink_to_fit();
            return jmp(nums, visit, nums.size() - 1, 0);
        }
};
