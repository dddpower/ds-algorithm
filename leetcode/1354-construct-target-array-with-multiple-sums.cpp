#include <bits/stdc++.h>
using namespace std;
class Solution {
public:
    bool isPossible(vector<int>& target) {
        sort(target.begin(), target.end(), greater<int>());
        vector<int> S;
        vector<int> a;
        int length = target.size();
        S.resize(length + 1);
        a.resize(length + 1);
        int i = 1;
        while (!target.empty()) {
            a[i] = S[i - 1] + length + 1 - i;
            const int& end = *target.rbegin();
            if (a[i] == end) {
                target.pop_back();
            } else if (a[i] > end) {
                return false;
            }
            S[i] = S[i - 1] + a[i];
            ++i;
        }
        return true;
    }
};

int main() {
    vector<int> test1{9,3,5};
    vector<int> test2{1,1,1,2};
    vector<int> test3{8,5};
    Solution s;
    cout << s.isPossible(test1) << '\n';
    cout << s.isPossible(test2) << '\n';
    cout << s.isPossible(test3) << '\n';
}
