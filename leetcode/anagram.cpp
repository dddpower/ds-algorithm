#define DEBUG
#ifdef DEBUG
#include <bits/stdc++.h>
using namespace std;
class Solution {
    public:
        vector<int> findAnagrams(string s, string p) {
        unordered_map<char, int> m{};
        decltype(m) current_m{};
        vector<int> result{};
        //init
        for (char ch = 'a'; ch <= 'z'; ++ch) {
            m[ch] = 0;
            current_m[ch] = 0;
        }

        for (auto ch : p) {
            ++m[ch];
        }
        // why size(s) not working?
        int p_len = p.length();
        int s_len = s.length();
        if (s_len >= p_len) {
            //initial map build
            for (int i = 0; i < p_len; ++i) {
                ++current_m[s[i]];
            }
            // string::iterator start = begin(s);
            // string::iterator end = advance(start, p_len);
            int start = 0;
            int end = p_len - 1;
            if (equal(m.begin(), m.end(), current_m.begin())) {
                result.emplace_back(start);
            }
            while (end < s_len - 1) {
                // update
                --current_m[s[start++]];
                ++current_m[s[++end]];
                // compare
                if (equal(m.begin(), m.end(), current_m.begin())) {
                    result.emplace_back(start);
                }
            }
        }
        return result;
    }
};
#endif
#ifdef DEBUG
int main() {
    string str{"cbaebabacd"};
    string input{"abc"};
    Solution s;
    auto r = s.findAnagrams(str, input);
    for_each(r.begin(), r.end(), [](int x){cout << x << " ";});
}
#endif
