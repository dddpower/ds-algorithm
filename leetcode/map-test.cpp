#include <map>
#include <unordered_map>
#include <iostream>
using namespace std;

int main() {
    map<int, int> m{};
    unordered_multimap<int, int> umm{};
    m.emplace(1, 1);
    m.emplace(2, 1);
    m.emplace(12, 3);
    
    umm.emplace(1, 1);
    umm.emplace(12, 3);
    umm.emplace(2, 1);
    umm.emplace(1, 3);
    for (auto i = begin(umm); i != end(umm); ++i) {
      cout << i->first << " " << i->second << endl;
    }
}
