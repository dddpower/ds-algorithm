#include <stdio.h>
#include <stdbool.h>
#define MAX 10000
int num1, num2;

int stack[MAX];
int top = -1;
bool isFull() {
  return top == MAX - 1;
}
bool isEmpty() {
  return top < 0;
}
void push(int val) {
  if (!isFull())
    stack[++top] = val;
  else {
    printf("stack is full\n");
  }
}
int pop() {
  if (!isEmpty())
    return stack[top--];
  printf("stack is empty\n");
  return -1;
}
bool bits1[MAX];
bool bits2[MAX];
int count(int x, bool* src, int len) {
  int c = 0;
  for (int i = 0; i < len; ++i) {
    if (src[i] == x) {
      ++c;
    }
  }
  return c;
}
void change(bool* src, int len) {
  for (int i = 0; i < len; ++i) {
    src[i] = !src[i];
  }
}
void print(bool* src, int len) {
  for (int i = 0; i < len; ++i) {
    printf("%d ", src[i]);
  }
  printf("\n");
}
void input() {
  scanf("%d %d", &num1, &num2);
  for (int i = 0; i < num1; ++i) {
    scanf("%d", &bits1[i]);
  }
  int b_i = 0;
  bool val = true;
  for (int i = 0; i < num2; ++i) {
    int len;
    scanf("%d", &len);
    for (int j = 0; j < len; ++j) {
      bits2[b_i++] = val;
    }
    val = !val;
  }
  // switch bits2's 0, 1, if wrong
  if (count(0, bits1, num1) != count(0, bits2, num1)) {
    change(bits2, num1);
  }
}

int solve() {
  int c = 0;
  int sum = 0;
  int left = -1;
  for (int i = 0; i < num1; ++i) {
    if ((bits1[i] != bits2[i])) {
      if (left == -1) {
        left = bits1[i];
        push(i);
      } else {
        if (bits1[i] == left) {
          push(i);
        } else {
          sum += i - pop();
          if (isEmpty())
            left = -1;
        }
      }
    }
  }
  // think about it
  // sum += c;
  return sum;
}

int main() {
  input();
  int answer = solve();
  int answer2;
  if (count(0, bits1, num1) == count(1, bits1, num1)) {
    change(bits2, num1);
    answer2 = solve();
    if (answer2 < answer) {
      answer = answer2;
    }
  }
  printf("%d", answer);
}
