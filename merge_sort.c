#include <stdbool.h>
#include <stdio.h>
#include <string.h>
#include <limits.h>
#define MAX_N 10000
int n;
unsigned in[MAX_N];
void print_arr(unsigned* src, int len) {
    for (int i = 0; i < len; ++i) {
        printf("%u ", src[i]);
    }
    printf("\n");
}
void input() {
    scanf("%d", &n);
    for (int i = 0; i < n; ++i) {
        scanf("%u", &in[i]);
    }
    // printf("input result\n");
    // print_arr(in, n);
}
int merge_sort(int l, int r) {
    // printf("starting from funciton call, we print src array\n");
    // print_arr(in + l, r - l + 1);
    if (l >= r)
        return 0;
    int m = l + (r - l) / 2;
    int c = merge_sort(l, m) + merge_sort(m + 1, r);
    // printf("starting from %d to %d, c = %d\n", l, r, c);
    // merge
    int l_len = m - l + 1;
    int r_len = r - m;
    // printf("l_len = %d, r_len = %d\n", l_len, r_len);
    unsigned l_arr[l_len];
    unsigned r_arr[r_len];
    memcpy(l_arr, &in[l], l_len * sizeof(unsigned));
    // printf("right array starts from %u\n", in[l_len]);
    memcpy(r_arr, &in[l + l_len], r_len * sizeof(unsigned));
    // printf("left array\n");
    // print_arr(l_arr, l_len);
    // printf("right array\n");
    // print_arr(r_arr, r_len);
    // printf("\n");
    int i = l;
    int l_i = 0;
    int r_i = 0;
    unsigned recent_right = UINT_MAX;
    while (l_i < l_len && r_i < r_len) {
        if (l_arr[l_i] <= r_arr[r_i]) {
            if (recent_right < l_arr[l_i]) {
                ++c;
                // printf("inversion occured at %d. recent right = %d\n", i, recent_right);
            }
            in[i++] = l_arr[l_i++];
        } else {
            recent_right = r_arr[r_i];
            in[i++] = r_arr[r_i++];
        }     
    }
    while (l_i < l_len) {
        if (recent_right < l_arr[l_i])
            ++c;
        in[i++] = l_arr[l_i++];
    }
    while (r_i < r_len)
        in[i++] = r_arr[r_i++];
    // printf("from %d to %d, number of inversions are %d.\n", l, r, c);
    return c;
}
int main() {
    input();
    int result = merge_sort(0, n - 1);
    printf("%d\n", result);
    // printf("sortresult: ");
    // print_arr(in, n);
}
