import java.{util => ju}
def last(xs: List[Any]): Any = 
    xs match
        case x :: Nil => x
        case _ :: x => last(x)
        case Nil => throw ju.NoSuchElementException("Empty List")

val l1 = List(1,2,3,4)
last(l1)
// last(List()

def penultimate(xs: List[Any]): Any = 
    xs match
        case a :: _ :: Nil => a
        case _ :: ys => penultimate(ys)
        case Nil => throw ju.NoSuchElementException()

penultimate(l1)
// penultimate(List(2))
penultimate(List(1,2))