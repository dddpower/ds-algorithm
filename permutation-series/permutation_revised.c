#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>
int* source;
int N;
void swap(int *a, int *b) {
  int t = *a;
  *a = *b;
  *b = t;
}

void print_source(int n) {
  for (int i = 0; i < n; ++i) {
    printf("%d ", source[i]);
  }
  printf("\n");
}

void permutation_distinct(int l, int r) {
  print_source(N);
  for (int i = l + 1; i <= r; ++i) {
    if (source[l] != source[i]) {
      swap(&source[l], &source[i]);
      permutation_distinct(l + 1, r);
      swap(&source[l], &source[i]);
    }
  }
}
int main() {
  scanf("%d", &N);
  source = malloc(sizeof(int) * N);
  for (int i = 0 ; i < N; ++i) {
    scanf("%d", &source[i]);
  }
  permutation_distinct(0, N - 1);
  free(source);
}
