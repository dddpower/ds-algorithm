#include <stdio.h>
#include <stdbool.h>
#define MAX 1000
bool visit[MAX];
void clear_visit(int n) {
  for (int i = 1; i <= n; ++i) {
    visit[i] = false;
  }
}
typedef struct _SimpleRecord {
  int record[MAX];
  bool visit[MAX];
  int ci;
} SimpleRecord;
SimpleRecord update(SimpleRecord sr, int value) {
  sr.visit[value] = true;
  sr.record[++sr.ci] = value;
  return sr;
}
void print_permutation(SimpleRecord sr, int c, int n) {
  if (c == n) {
    for (int i = 0; i < n; ++i) {
      printf("%d ", sr.record[i]);
    }
    printf("\n");
    return;
  }
  for (int i = 1; i <= n; ++i) {
    if (!sr.visit[i]) {
      print_permutation(update(sr, i), c + 1, n);
    }
  }
}
int main() {
  SimpleRecord sr;
  for (int i = 1; i <= 5; ++i) {
    sr.visit[i] = false;
    sr.ci = -1;
  }
  print_permutation(sr, 0, 5);
}
