#include <stdio.h>
#define NUMSTAIRS 301
#define MAXSCORE 10000
int stepscore[NUMSTAIRS];
int memory[NUMSTAIRS][3];
int max2(int a, int b) {
  return a < b ? b : a;
}
int max3(int a, int b, int c) {
  return max2(max2(a, b), c);
}
int greatest(int index) {
  return max3(memory[index][0], memory[index][1], memory[index][2]);
}
int climb_stairs(int n) {
  memory[2][0] = stepscore[1] + stepscore[2];
  memory[2][1] = stepscore[2];
  if (n == 1) {
    return stepscore[1];
  }
  if (n == 2) {
    return stepscore[1] + stepscore[2];
  }
  for (int i = 3; i <= n; ++i) {
    memory[i][0] = memory[i - 2][2] + stepscore[i - 1] + stepscore[i];
    memory[i][1] = greatest(i - 3) + stepscore[i - 1] + stepscore[i];
    memory[i][2] = greatest(i - 2) + stepscore[i];
  }
  return greatest(n);
}
int main() {
  int N;
  scanf("%d", &N);
  for (int i = 1; i <= N; ++i) {
    scanf("%d", &stepscore[i]);
  }
  int ans = climb_stairs(N);
  printf("%d\n", ans);
}
