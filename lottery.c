#include <stdio.h>
#include <stdlib.h>
#include <string.h>
enum {
    N = 50000,
    less_than,
    greater_than
};
int p, s;
int pt[N];
int seg_from[N];
int seg_to[N];
void input() {
    scanf("%d %d", &s, &p);
    for (int i = 0; i < s; ++i) {
        scanf("%d %d", &seg_from[i], &seg_to[i]);
    }
    for (int i = 0; i < p; ++i) {
        scanf("%d", &pt[i]);
    }
}

int compare(const void* first, const void* second) {
    return *(const int*)first - *(const int*)second;
}
int index_bsearch(int key, int state, int* src, int l, int r) {
    // l & r are reversed
    if (l > r) {
        if (state == less_than)
            return r;
        return l;
    }
    int m = l + (r - l) / 2;
    if (key == src[m]) {
        if (state == less_than)
            return index_bsearch(key, state, src, l, m - 1);
        return index_bsearch(key, state, src, m + 1, r);
    }
    if (key > src[m])
        return index_bsearch(key, state, src, m + 1, r);
    return index_bsearch(key, state, src, l, m - 1);
}
void solve() {
   // sort each from, to array by increasing order
   qsort(seg_from, s, sizeof(int), compare);
   qsort(seg_to, s, sizeof(int), compare);
   for (int i = 0; i < p; ++i) {
        int greater_index = index_bsearch(pt[i], greater_than, seg_from, 0, s - 1);
        int less_index = index_bsearch(pt[i], less_than, seg_to, 0, s - 1);
        char message[100] = "current pt = %d, out from index: %d out to index: %d\n";
        // printf(message, pt[i], greater_index, less_index);
        printf("%d ",greater_index - less_index - 1);
   }
   printf("\n");
}
int main() {
    input();
    solve();
}
