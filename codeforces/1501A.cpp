#include <bits/stdc++.h>
using namespace std;
size_t problem() {
  // begin input
  size_t n;
  cin >> n;
  vector<size_t> a(n + 1), b(n + 1), tm(n + 1);
  for (size_t i = 1; i <= n; ++i) {
    cin >> a[i] >> b[i];
  }
  for (size_t i = 1; i <= n; ++i) {
    cin >> tm[i];
  }
  // end input
  
  //begin solution
  size_t arrival_time{0};
  size_t leaving_time{0};
  for (size_t i = 1; i <= n; ++i) {
    //train arrives at 
    arrival_time = leaving_time + a[i] - b[i - 1] + tm[i];

    // train leaves at
    auto val = b[i] - a[i];
    val = val % 2 + val / 2;
    leaving_time = max(arrival_time + val, b[i]);
  }
  return arrival_time;
}
int main() {
  size_t numtest;
  cin >> numtest;
  for (size_t i = 0; i < numtest; ++i) {
    auto sol = problem();
    cout << sol << endl;
  }
}
