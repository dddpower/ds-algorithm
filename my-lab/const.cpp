class foo {
  mutable int val;
  public:
  foo() {
    val = 0;
  }
  int increase_getVal() const {
    val += 1;
    return val;
  }
  const int getVal2() {
    val += 1;
    return val;
  }
};
int main() {
  int a = 3;
  int b = 4;
  int* const ptr = &a;
  const foo f;
  f.increase_getVal();
  *ptr = 4;
  // ptr = &b;
}
