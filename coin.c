#include <stdio.h>
#include <string.h>
#define SCAN_INT(X) scanf("%d", &X)
#define PRINT_INT(X) printf("%d\n", X)
#define N 1001
#define K 10001
int n, k;
int in[N];
int temp[K];
int mem[K];
void input() {
  SCAN_INT(n);
  SCAN_INT(k);
  for (int i = 1; i <= n; ++i) {
    SCAN_INT(in[i]);
  }
}

int solve_dp() {
  mem[0] = 1;
  for (int i = 1; i <= n; ++i) {
    memmove(temp, mem, sizeof(mem));
    for (int j = in[i]; j <= k; ++j) {
      mem[j] = temp[j] + mem[j - in[i]];
    }
  }
  return mem[k];
}
// void print_mem() {
//   printf("table status\n");
//   for (int i = 1; i <= n; ++i) {
//     for (int j = 1; j <= k; ++j) {
//       printf("%d ", mem[i][j]);
//     }
//     printf("\n");
//   }
// }
int main() {
  input();
  int answer = solve_dp();
  PRINT_INT(answer);
}
