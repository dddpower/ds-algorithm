#include <stdio.h>
#include <stdbool.h>
#define MAX 510
int N;
int values[MAX][MAX];
int memory[MAX][MAX];
int max2(int a, int b) {
  return a < b ? b : a;
}
int get_max(int index) {
  int max = 0;
  for (int i = 1; i <= index; ++i) {
    max = max > memory[index][i] ? max : memory[index][i];
  }
  return max;
}
int int_triangle(int n) {
  for (int i = 1; i <= n; ++i) {
    for (int j = 1; j <= i; ++j) {
      memory[i][j] = max2(memory[i - 1][j - 1], memory[i - 1][j]) + values[i][j];
    }
  }
  return get_max(n);
}
int main() {
  scanf("%d", &N);
  for (int i = 1; i <= N; ++i) {
    for (int j = 1; j <=i; ++j) {
      scanf("%d", &values[i][j]);
    }
  }
  int ans = int_triangle(N);
  printf("%d\n", ans);
}
