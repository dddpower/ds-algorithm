#include <stdio.h>
#include <limits.h>
#define LEN 100000
int N;
int in[LEN];
int mem[LEN];
void print_memory() {
  printf("print memory\n");
  for (int i = 0; i < N; ++i) {
    printf("%d ", mem[i]);
  }
  printf("\n");
}
int max2(int a, int b) {
  return a < b ? b : a;
}
int consecutive_sum() {
  mem[0] = in[0];
  for (int i = 1; i < N; ++i) {
    mem[i] = max2(mem[i - 1] + in[i], in[i]);
  }
  int max = -1000;
  for (int i = 0; i < N; ++i) {
    max = max < mem[i] ? mem[i] : max;
  }
  return max;
}
void input() {
  scanf("%d", &N);
  for (int i = 0; i < N; ++i) {
    scanf("%d", &in[i]);
  }
}
int main() {
  input();
  int answer = consecutive_sum();
  printf("%d\n", answer);
}
