#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#define NUM 1000
int N;
int src[NUM];
int table[NUM];

int count = 0;
int solve(int x) {
  // make table from zero to the x
  
  // int val = table[x];
  if (!x) {
    return 1;
  } else {
    int val1 = 0, val2 = 0, val3 = 0;
    if (x - 3 >= 0) {
      val3 = solve(x - 3);
    }
    if (x - 2 >= 0) {
      val2 = solve(x - 2);
    } 
    if (x - 1 >= 0) {
      val1 = solve(x - 1);
    }
    return val1 + val2 + val3;
  }
}
void solve_another(int x) {
  if (x - 1 >= 0) {
    if (x - 3 >= 0) {
      printf(" + 3");
      solve_another(x - 3);
    }
    if (x - 2 >= 0) {
      printf(" + 2");
      solve_another(x - 2);
    }
    if (x - 1 >= 0) {
      printf(" + 1");
      solve_another(x - 1);
    }
  } else {
    printf("\n");
  }
}
int main(int argc, char** argv) {
  // scanf("%d", &N);
  assert(argc == 2);
  int k = atoi(argv[1]);
  printf("input %d\n", k);
  int ans = solve(k);
  printf("%d\n", ans);
  solve_another(k);
}
