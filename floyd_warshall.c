#include <stdio.h>
#include <limits.h>
#define MAX_V 101
#define MAX_E 100001
#define PRINT_INT(X) printf("%d", X);
#define PRINTLN printf("\n");
#define INPUT_INT(X) scanf("%d", &X);
// watch out the overflow!
#define INF INT_MAX
int n, m;
int edge[MAX_V][MAX_V];
void print_edges() {
    for (int i = 1; i <= n; ++i) {
        for (int j = 1; j <= n; ++j) {
            PRINT_INT(edge[i][j])
            printf("\t");
        }
        PRINTLN
    }
    PRINTLN
}
void input() {
    INPUT_INT(n)
    INPUT_INT(m)
    // init edge to INF
    for (int i = 1; i <= n; ++i) {
        for (int j = 1; j <= n; ++j) {
            edge[i][j] = INF;
        }
    }
    for (int i = 0; i < m; ++i) {
        int a, b, c;
        INPUT_INT(a)
        INPUT_INT(b)
        INPUT_INT(c)
        edge[a][b] = edge[a][b] < c ? edge[a][b] : c;
    }
}
void floyd_warshall() {
    for (int i = 1; i <= n; ++i) {
        for (int j = 1; j <= n; ++j) {
            for (int k = 1; k <= n; ++k) {
                int val0 = edge[j][i];
                int val1 = edge[i][k];
                if (val0 != INF && val1 != INF && j != k) {
                    int val = val0 + val1;
                    edge[j][k] = edge[j][k] > val ? val : edge[j][k];
                }
            }
        }
    }
}
void print_result() {
    for (int i = 1; i <= n; ++i) {
        for (int j = 1; j <= n; ++j) {
            int val = edge[i][j];
            if (val == INF)
                printf("0 ");
            else printf("%d ", val);
        }
        PRINTLN
    }
    PRINTLN
}
int main() {
    input();
    floyd_warshall();
    print_result();
}
