#include <bits/stdc++.h>
using namespace std;

size_t width(int d, int x) {
  if (x < 0 || x > 9)
    return 0;
  
  if (d == 1) {
    if (x == 0)
      return 0;
    return 1;
  }
  return width(d - 1, x - 1) + width(d - 1, x + 1);
}
size_t width(int d) {
  size_t sum;
  for (int i = 0; i <= 9; ++i) {
    sum += width(d, i);
  }
  return sum;
}

// depth, remains
pair<size_t, size_t> get_depth(size_t k) {
  size_t i = 0;
  while (k >= 0) {
    k -= width(++i);
  }
  return make_pair(i, k + width(i));
}
void solve(size_t k) {
  auto d = get_depth(k);
  int i = 1;
  while (d.second >= 0) {
    d.second -= width(d.first, i++);
  }
  width(d.first - 1)
}
int main() {
  size_t k;
  cin >> k;
}
