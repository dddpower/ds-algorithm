#include <bits/stdc++.h>
using namespace std;

int kth_multi(int a, int b, int k) {
  int next_a{a};
  int next_b{b};
  int current{next_a};
  // first iteration
  next_a += a;
  --k;

  // second ~ Kth iteration
  while (k) {
    if (next_a < next_b) {
      current = next_a;
      next_a += a;
    } else if (next_a > next_b){
      current = next_b;
      next_b += b;
    } else {
      current = next_a;
      next_a += a;
      next_b += b;
    }
    --k;
  }
  return current; // current updates at every iteration.
  // time complexity : O(N)
}

int main() {
  int a, b, k;
  cin >> a >> b >> k;
  int result = kth_multi(a, b, k);
  cout << result << endl;
}
