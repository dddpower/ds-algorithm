#include <stdio.h>
#include <string.h>
#define LEN 1001
char in1[LEN];
char in2[LEN];
int mem[LEN][LEN];
void input() {
  scanf("%s", in1);
  scanf("%s", in2);
}
int max2(int a, int b) {
  return a < b ? b : a;
}
int LCS() {
  int len1 = strlen(in1);
  int len2 = strlen(in2);
  for (int i = 0; i < len1; ++i) {
    for (int j = 0; j < len2; ++j) {
      int val1 = i ? mem[i - 1][j] : 0;
      int val2 = j ? mem[i][j - 1] : 0;
      int val3 = i && j ? mem[i - 1][j - 1] : 0;
      if (in1[i] == in2[j]) {
        mem[i][j] = val3 + 1;
      } else {
        mem[i][j] = max2(val1, val2);
      }
    }
  }
  return mem[len1 - 1][len2 - 1];
}
int main() {
  input();
  int ans = LCS();
  printf("%d\n", ans);
}
