// #include <vector>
// #include <iostream>
#include <bits/stdc++.h>
#include <assert.h>

using namespace std;

typedef struct _node node;
struct _node {
    node* left;
    node* right;
    int x;
    int y;
};

void print_tree_preorder(node* n);
node* insert(node* n, vector<int> data) {
  if (!n) {
    n = new node();
    n->x = data[0];
    n->y = data[1];
  } else {
    assert(n->x != data[0]);
    if (n->x < data[0]) {
    n->right = insert(n->right, data);
    } else {
      n->left = insert(n->left, data);
    }
  }
  
  return n;
}

void print_dot(vector<int> dot) {
    cout << "(" << dot[0] << ", " << dot[1] << ")";
}
void print_dot_vector(vector<vector<int>> x) {
    for_each(x.begin(), x.end(), [](vector<int> a) {
        print_dot(a);
        cout << " ";
    });
    cout << endl;
}

void print_tree_preorder(node* n) {
    if(n) {
        vector<int> v;
        v.push_back(n->x);
        v.push_back(n->y);
        print_dot(v);
        print_tree_preorder(n->left);
        print_tree_preorder(n->right);
    }
}
void print_tree_postorder(node* n) {
    if (n) {
        print_tree_postorder(n->left);
        print_tree_postorder(n->right);
        vector<int> v;
        v.push_back(n->x);
        v.push_back(n->y);
        print_dot(v);
    }
}
vector<vector<int>> solution(vector<vector<int>> nodeinfo) {
    vector<vector<int>> answer;
    
    // sort using a lambda expression 
    sort(nodeinfo.begin(), nodeinfo.end(),
        [](vector<int> a, vector<int> b) {
            return a[1] > b[1];});
    print_dot_vector(nodeinfo);
    
    auto it = nodeinfo.begin();
    node* root = NULL;
    while (it != nodeinfo.end()) {
      if (!root) {
        root = insert(root, *it);
      } else {
        insert(root, *it);
      }
      ++it;
    }
    cout << "passed\n";
    
    print_tree_preorder(root);
    return answer;
}

int main() {
    vector<int> v;
    vector<vector<int>> source;
    v.push_back(5);
    v.push_back(3);
    source.push_back(v);
    v.clear();
    v.push_back(11);
    v.push_back(5);
    source.push_back(v);
    v.clear();
    v.push_back(13);
    v.push_back(3);
    source.push_back(v);
    v.clear();
    v.push_back(3);
    v.push_back(5);
    source.push_back(v);
    v.clear();
    v.push_back(6);
    v.push_back(1);
    source.push_back(v);
    v.clear();
    v.push_back(1);
    v.push_back(3);
    source.push_back(v);
    v.clear();
    v.push_back(8);
    v.push_back(6);
    source.push_back(v);
    v.clear();
    v.push_back(7);
    v.push_back(2);
    source.push_back(v);
    v.clear();
    v.push_back(2);
    v.push_back(2);
    source.push_back(v);
    v.clear();
    
    solution(source);
}
