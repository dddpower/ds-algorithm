#include <stdio.h>
#include <assert.h>
#define MAX 10001
#define AMOUNT 1000 // WARNING:amount can be 0

int mem[MAX];
int wine[MAX];
int max2(int a, int b) {
  return a < b ? b : a;
}
int max3(int a, int b, int c) {
  return max2(max2(a, b), c);
}

int drink_wine(int n) {
  mem[1] = wine[1];
  mem[2] = wine[1] + wine[2];
  for (int i = 3; i <= n; ++i)
    mem[i] = max3(mem[i - 1], mem[i - 2] + wine[i], mem[i - 3] + wine[i - 1] + wine[i]);
  return mem[n];
}
int main() {
  int N;
  scanf("%d", &N);
  for (int i = 1; i <= N; ++i) {
    scanf("%d", &wine[i]);
  }
  int ans = drink_wine(N);
  printf("%d\n", ans);
}
