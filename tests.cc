#include "quicksort.cc"
#include <algorithm>
#include <gtest/gtest.h>
#include <string>

TEST(InsertionSortTest,  isWellSorted) {
  std::vector<int> sample1 = {4, 5, 3, 1, 9, 9, 9, 5, 6, 7};
  std::vector<int> sample2 = {4, 5, 3, 1, 9, 9, 9, 5, 6, 7};

  std::sort(sample1.begin(), sample1.end());
  InsertionSort(sample2);
  auto output1 = toString(sample1);
  auto output2 = toString(sample2);
  ASSERT_STREQ(output1.c_str(), output2.c_str());
}

int main(int argc, char **argv) {
    testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}