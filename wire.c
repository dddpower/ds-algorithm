#include <stdio.h>
#include <stdlib.h>

#define NUMWIRE 101
typedef struct {
  int first;
  int second;
} pair;
int N;
pair in[NUMWIRE];
int mem[NUMWIRE];

int compare_pairs(const void* a, const void* b)
{
    int arg1 = ((const pair*)a)->first;
    int arg2 = ((const pair*)b)->first;
 
    if (arg1 < arg2) return -1;
    if (arg1 > arg2) return 1;
    return 0;
 
    // return (arg1 > arg2) - (arg1 < arg2); // possible shortcut
    // return arg1 - arg2; // erroneous shortcut (fails if INT_MIN is present)
}
void input() {
  scanf("%d", &N);
  for (int i = 1; i <= N; ++i) {
    scanf("%d %d", &in[i].first, &in[i].second);
  }
}
int wire() {
  qsort(&in[1], N, sizeof(pair), compare_pairs);
  for (int i = 1; i <= N; ++i) {
    mem[i] = mem[i - 1] + (in[i - 1].second > in[i].second);
  }
  return mem[N];
}
int main() {
  input();
  int sol = wire();
  printf("%d\n", sol);
}
