#include <stdio.h>
#define NUM 10001
#define SCORE 1000
int mem[NUM][2];    // 0    1   1 
                    // 1    0   1
                    // 1    1   0
int wine[NUM];
int max2(int a, int b) {
    return a < b ? b : a;
}
int drink_wine_my(int n) {
    // mem[1][0] = 0;
    mem[1][1] = wine[1];
    mem[2][0] = wine[1];
    mem[2][1] = wine[1] + wine[2];
    for (int i = 3; i <= n; ++i) {
        mem[i][0] = mem[i - 3][0] + wine[i - 2] + wine[i - 1];
        mem[i][1] = wine[i] + max2(mem[i - 2][0] + wine[i - 1], mem[i - 1][0]);
    }
    return max2(mem[n][0], mem[n][1]);
}
int main()
{
  int n;
  scanf("%d", &n);
  for (int i = 1; i <= n; ++i) {
      scanf("%d", &wine[i]);
  }
  int ans = drink_wine_my(n);
  printf("%d\n", ans);
}

