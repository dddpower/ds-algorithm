#include <iostream>
#include <vector>
#include <string>
#include <numeric>

const int INSERTIONSORTCUTOFF = 8;

  std::string toString(std::vector<int> source) {
  std::iota(source.begin(), source.end(), '0');
  std::string str(source.begin(), source.end());
  return str;
}
int Medianof3(std::vector<int> source) {
  auto begin = source.begin();
  auto last = source.rbegin();
  auto middle = begin + source.size() / 2;
  if (*begin > *middle) {
    std::swap(*begin, *last);
  }
  if (*begin > *last) {
    std::swap(*begin, *last);
  }
  if (*middle > *last) {
    std::swap(*middle, *last);
  }
  std::swap(*begin, *middle);
  int pivot = *begin;
  return pivot; // \*begin is pivot
}
void InsertionSort(std::vector<int> source) {
  for (auto i = source.begin(); i != source.end() - 1; ++i) {
    for (auto j = i + 1; j > source.begin() && *(j - 1) > *j; --j) {
      std::swap(*(j - 1), *j);
    }
  }
}
void BentleyMcIlroy3wayPartitionQuickSort
    (std::vector<int> source, int from, int to) {
  int sourcelength = source.size();
  if (sourcelength <= INSERTIONSORTCUTOFF) {
    InsertionSort(source);
  } else {
    int pivot = Medianof3(source);
    auto l_iter = source.begin();
    auto r_iter = source.rbegin();
    auto same_l_iter = source.end();
    auto same_r_iter = source.end();
    while (true) {
      while (*++l_iter < pivot) {
        if (l_iter == source.end()) {
          break;
        }
      }
      while (*--r_iter > pivot) {
        ;
      }
      if (&l_iter == &r_iter && *l_iter == pivot) {
        std::swap(*++same_l_iter, l_iter);
      }
      if (l_iter >= r_iter) {
        break;
      }
      std::swap(*l_iter, *r_iter);
      
    }
  }
}
