#include <iostream>
using namespace std;
int main() {
    constexpr int a = 3;
    int b = 4;
    if constexpr(!a) {
        b /= 0;
    }
    std::cout << a << std::endl;
}
