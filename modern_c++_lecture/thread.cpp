#include <iostream>
#include <thread>
using namespace std;
void foo() {
  cout << "thread start\n";
  std::this_thread::sleep_for(2s);
  cout << "thread end" << endl;
}

int main() {
  thread t(&foo);
  // t.detach();
  t.join();
}
