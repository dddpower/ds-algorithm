#include <iostream>
using std::cout;
using std::endl;
// containter
template<typename T>
struct Node {
    T data;
    Node* next;
    Node(const T& a, Node* n) : data(a), next(n) {}
};

// iterator
template<typename T>
class siterator {
    Node<T>* current;
    public:
    siterator(Node<T>* p = 0) : current(p) {}
    siterator& operator ++() {
        current = current->next;
        return *this;
    }
    bool operator == (siterator<T> const & that) {
        return current == that.current;
    }
    T& operator *() {
        return current->data;
    }
    bool operator != (const siterator<T>& that) {
        return current != that.current;
    }
};


// containter
template<typename T>
class sList {
    Node<T> * head{0};
    public:
    void push_front(const T& a) {
        head = new Node<T>(a, head);
    }
    using iterator = siterator<T>;
    iterator begin() {
        return iterator(head);
    }
    iterator end() {
        return iterator(0);
    }
};

// algorithm
template<typename T, typename S>
// half range[)
T efind(T first, T last, S element) {
    T el = first;
    while (el != last) {
        if (*el == element)
            return el;
        ++el;
    }
    return last;
}

int main() {
    sList<int> s;
    s.push_front(10);
    s.push_front(20);
    s.push_front(30);
    s.push_front(40);
    sList<int>::iterator it = s.begin();
    sList<int>::iterator it2 = efind(s.begin(), s.end(), 20);
    if (it2 == s.end()) {
        cout << "fail" << endl;
    } else {
        cout << *it2 << endl;
    }
}
