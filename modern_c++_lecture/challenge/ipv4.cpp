#include <iostream>
#include <array>
#include <sstream>
#include <cassert>

class ipv4 {
    using bits8 = unsigned char;
    std::array<bits8, 4> data;
    public:
    constexpr ipv4() : data{0} {}
    constexpr ipv4(bits8 const a, bits8 const b, bits8 const c, bits8 const d) : 
        data{a, b, c, d}{}
    explicit constexpr ipv4(unsigned long a);
    ipv4(ipv4 const & other) noexcept : data(other.data) {}
    ipv4 &operator=(ipv4 const & other) noexcept {
        data = other.data;
        return *this;
    }

    std::string to_string() const {
        std::stringstream sstr;
        sstr << *this;
        return sstr.str();
    }
};
