// Write a class named Ball. Ball should have two private member variables with default values: m_color (“black”) and m_radius (10.0). Ball should provide constructors to set only m_color, set only m_radius, set both, or set neither value. For this quiz question, do not use default parameters for your constructors. Also write a function to print out the color and radius of the ball.
// and produce the result:
// 
// color: black, radius: 10
// color: blue, radius: 10
// color: black, radius: 20
// color: blue, radius: 20
#include <bits/stdc++.h>
class Ball {
    std::string m_color;
    double m_radius;
    public:
    Ball(const std::string& str = "black", double r = 10) :
        m_color{str}, m_radius{r} {}
    Ball (double r) : m_color{"black"}, m_radius{r} {}
    void print () {
        std::cout << "color: " << m_color << ", radius: " << m_radius << "\n";
    }
};
// The following sample program should compile:
int main()
{
	Ball def{};
	def.print();
 
	Ball blue{ "blue" };
	blue.print();
	
	Ball twenty{ 20.0 };
	twenty.print();
	
	Ball blueTwenty{ "blue", 20.0 };
	blueTwenty.print();
 
	return 0;
}

