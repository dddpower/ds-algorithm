class Simple {
  int m_id;
  public:
  Simple(int id) {
    setID(id);
  }
  void setID(int id) {
    m_id = id;
  }
  // setID is converted to 
  // setID(Simple* const this, int id) {
  //    this->m_id = id;
  // }
  int getID() {
    return m_id;
  }
  // getID(Simple* const this, m_id) {
  //    return this->m_id;
  // }
}
