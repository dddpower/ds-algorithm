#include <vector>
class MyClass
{
private:
    static std::vector<char> s_mychars;
};
 
std::vector<char> MyClass::s_mychars{
  []{ // The parameter list of lambdas without parameters can be omitted.
      // Inside the lambda we can declare another vector and use a loop.
      std::vector<char> v{};
      
      for (char ch{ 'a' }; ch <= 'z'; ++ch)
      {
          v.push_back(ch);
      }
      
      return v;
  }() // Call the lambda right away
};

int main() {
    std::vector<int> v = {
        []{
            std::vector<int> v1{};
            for (int i{0}; i < 10; ++i) {
                v1.emplace_back(i);
            }
            return v1;
        }()
    };
}
