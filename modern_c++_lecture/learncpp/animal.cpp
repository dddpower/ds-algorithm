#include <iostream>
#include <string>
#include <string_view>
using namespace std;

class Animal {
    protected:
        string m_name;
        string m_speak;
        explicit Animal(const string &name) :  m_name{name}{
        }
    public:
        const string& getName() const {
            return m_name;
        }
        string_view speak() const {
            return "???";
        }
};
class Cat : public Animal {
  public:
  explicit Cat(const string& name) : Animal{name} {}
  string_view speak() const {return "Meow";}
};

class Dog: public Animal {
  public:
  explicit Dog(const string& name) : Animal{name}{}
  string_view speak() const {return "woof";}
};
int main() {

}
