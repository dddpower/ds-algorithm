//define empty class
class input_iterator {};
class forward_iterator : public input_iterator {};
class bidirectional_iterator : public forward_iterator {};
class random_access_iterator : public bidirectional_iterator {};


template <typename T>
struct Node {
    T value;
    T* next;
};
template <typename T>
class mylist {
    using value_type = T;
    using iterator_tag = bidirectional_iterator;
    Node<T>* head;
    void add_front(T val) {
        head = new Node<T>{val, head};
    }
    Node<T>* begin() {
        return head;
    }
    Node<T>* end() {
        
    }
};
class mylist_iterator {
    using tag = input_iterator;
};

template<typename T>
class myvector_iterator {
    using tag = random_access_iterator;
};

void eadvance_imp(mylist_iterator& i, int n) {
    while (n--) {
        ++i;
    }
}

void edvance_imp(myvector_iterator& i, int n) {
    i = i + n;
}

// in algorighm libray
template<typename iteratorT>
void eadvance(iteratorT i, int n) {
    i::tag
}
