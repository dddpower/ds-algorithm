#include <list>
#include <iostream>
// #include <algorithm>
using namespace std;

template<typename T> struct my_iterator_traits {
    using value_type = typename T::value_type;
};
template<typename T> struct my_iterator_traits<T*> {
    using value_type = T;
};
template<typename T>
typename T::value_type sum(T first, T last) {
    typename my_iterator_traits<T>::value_type s = 0;
    // from C++11
    // error : decltype(*first) = T& 가 된다.
    // decltype(*first) s11 = 0;
    typename
        remove_reference<decltype(*first)>::type s11 = 0;
    while (first != last) {
        s = s + *first;
        ++first;
    }
    return s;
}

int main() {
    list<int> s = {1, 2, 3, 4, 5, 6, 7};
    int n = sum(begin(s), end(s));
    cout << n << endl;
}
