#include <iostream>
class Base {
    public:
    Base() {
        std::cout << "Base Class Constructor" << std::endl;
    }
    virtual ~Base() {
        std::cout << "Base Class Destructor" << std::endl;
    }
};
class Derived : public Base {
    public:
    Derived() {
        std::cout << "Derived Class Constructor" << std::endl;
    }
    void greet() {
        std::cout << "greeting derived" << std::endl;
    }
    // ~Derived() {
    //     std::cout << "Derived Class Destructor" << std::endl;
    // }
};
class NonVirtualBase {
    public:
    void greet() {
        std::cout << "Nonvirtual Base greeting\n";
    }
};
class NonVirtualDerived : public NonVirtualBase {
    public:
    void greet() {
        std::cout << "Nonvirtual Derived greeting\n";
    }
};
int main() {
    // std::cout << "case 1" << std::endl;
    // Derived d;
    // std::cout << std::endl;
    // std::cout << "case 2" << std::endl;
    // Base* b_ptr = new Derived();
    // delete b_ptr;
    std::cout << "case 3" << std::endl;
    Base* b_ptr = new Derived();
    dynamic_cast<Derived*>(b_ptr)->greet();
    delete b_ptr;
    NonVirtualBase* nb = new NonVirtualDerived{};
    static_cast<NonVirtualDerived*>(nb)->greet();
    nb->greet();
}

