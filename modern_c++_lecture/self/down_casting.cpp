#include <iostream>
#include <string>

class Base {
  std::string s;

 public:
  Base() : s("base") { std::cout << "base class" << std::endl; }

  void what() { std::cout << s << std::endl; }
};
class Derived : public Base {
  std::string s;

 public:
  Derived() : s("derived"), Base() { std::cout << "derived class" << std::endl; }

  void what() { std::cout << s << std::endl; }
};
int main() {
  Base p;
  Derived c;

  std::cout << "pointer ver" << std::endl;
  Base* p_p = &p;

  Derived* p_c = static_cast<Derived*>(p_p);
  p_c->what();

  return 0;
}
