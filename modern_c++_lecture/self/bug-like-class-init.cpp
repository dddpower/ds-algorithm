class Fraction {
    int numerator;
    int denominator;
    public:
    // Fraction() : numerator{1}, denominator{1} {}
    Fraction() = default;
    Fraction(int n, int d = 1) : numerator{n}, denominator{d} {}
};

int main() {
    Fraction f = 6;
    Fraction d;
    Fraction e{};
}
