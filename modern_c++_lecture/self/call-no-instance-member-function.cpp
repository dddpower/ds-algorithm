#include <iostream>
class foo {
  public:
    void print() {
      std::cout << "print function\n";
    }
};

int main() {
  foo::print();
}
