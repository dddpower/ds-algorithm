#include <iostream>
using namespace std;

class A {
    int a;
    protected:
    int b;
    public: 
    int c;
};
class B : public A {
    public:
    // A this;
};
class C : protected A {
    protected:
        //A this;
};
int main() {

}
