#include <bits/stdc++.h>
// pair.first = node number, pair.second = value
using tree = std::map<int, std::vector<std::pair<int, int>>>;
using visit = std::vector<bool>;
tree t{};
int n;
void input() {
  int a, b, c;
  std::cin >> n;
  for (int i = 0; i < n; ++i) {
    std::cin >> a;
    while (true)
    {
      std::cin >> b;
      if (b == -1)
      {
        t[a].shrink_to_fit();
        break;
      }
      std::cin >> c;
      t[a].emplace_back(std::make_pair(b, c));
    }
  }
}

int node_num, max;
void far_dfs(int n, visit& v, int acc){
  v[n] = true;
  for (auto adj_v : t[n]) {
    int c_n = adj_v.first;
    if (!v[c_n]) {
      far_dfs(c_n, v, acc + adj_v.second);
    }
  }
  // reach to the leaf
  if (max < acc) {
    max = acc;
    node_num = n;
  }
  v[n] = false;
}
int solve() {
  // 1. Find the most far vertex.
  // Just for simplicity, we start from vertex number 1. Any node can be root.
  visit v{};
  v.resize(n + 1);
  v.shrink_to_fit();
  // v[1] = true;
  far_dfs(1, v, 0);
  // v[1] = false;
  // std::cout << "from 1, longest node = " << node_num << '\n';
  // std::cout << "distance  = " << max << '\n';
  max = 0;
  int new_n = node_num;
  // v[new_n] = true;
  far_dfs(new_n, v, 0);
  // v[new_n] = false;
  // std::cout << "from " << new_n << ", longest node = " << node_num << '\n';
  // std::cout << "distance  = " << max << '\n';
  return max;
}


int main() {
  input();
  int result = solve();
  std::cout<< result << '\n';
}

