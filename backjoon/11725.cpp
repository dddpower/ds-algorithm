#include <bits/stdc++.h>
using graph = std::map<int, std::vector<int>>;
// vector storing parent node number
std::vector<int> result{};
graph g{}; 
int n;
void input() {
  std::cin >> n;
  result.resize(n + 1);
  int a{}, b{};
  for (int i = 1; i < n; ++i)
  {
    std::cin >> a >> b;
    g[a].emplace_back(b);
    g[b].emplace_back(a);
  }
  result[1] = 1; //root setting
}
void solve(int const &key) {
  for (auto n : g[key]) {
    if(!result[n]) {
      result[n] = key;
      solve(n);
    }
  }
}

int main() {
  input();
  solve(1);
  std::for_each(result.begin() + 2, result.end(), [](int const& x) {
    std::cout << x << '\n';
  });
}