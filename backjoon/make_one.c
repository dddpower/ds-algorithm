#include <stdio.h>
#define BIGNUM 1000000
int N;
int table[BIGNUM];
int min(int a, int b) {
  return a < b ? a : b;
}
int min_3(int a, int b, int c) {
  return min(min(a, b), c);
}
int devide(int x, int a) {
  return !(x % a);
}
void make_table() {
  table[1] = 0;
  for (int i = 2; i <= N; ++i) {
    int val2 = BIGNUM;
    int val3 = BIGNUM;
    int min_val = table[i - 1];
    if (devide(i, 2)) {
      min_val = min(min_val, table[i / 2]);
    }
    if (devide(i, 3)) {
      min_val = min(min_val, table[i / 3]);
    }
    table[i] = min_val + 1;
  }
}
int main() {
  scanf("%d", &N);
  make_table();
  printf("%d\n", table[N]);
}
