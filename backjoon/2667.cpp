#include <stack>
#include <queue>
#include <vector>
#include <iostream>
#include <algorithm>
using namespace std;


int n;
vector<string> map;
vector<vector<bool>> visit;
vector<int> group;
void input() {
  cin >> n;
  string str;
  visit.resize(n);
  for (auto i = visit.begin(); i != visit.end(); ++i) {
    i->resize(n);
    for (auto j = i->begin(); j != i->end(); ++j) {
      *j = false;
    }
  }
  for (int i = 0; i < n; ++i) {
    cin >> str;
    map.push_back(str);
  }
}
bool is_inbound(int val) {
  return val >= 0 && val < n;
}
bool fit_condition(int row, int col) {
  return is_inbound(row) && is_inbound(col) && map[row][col] == '1' &&
    !visit[row][col];
}
int my_counter = 0;
void solve_recursive(int row, int col) {
  ++my_counter;
  if (fit_condition(row, col + 1))
  {
    visit[row][col + 1] = true;
    solve_recursive(row, col + 1);
  }
  if (fit_condition(row, col - 1))
  {
    visit[row][col - 1] = true;
    solve_recursive(row, col - 1);
  }
  if (fit_condition(row + 1, col))
  {
    visit[row + 1][col] = true;
    solve_recursive(row + 1, col);
  }
  if (fit_condition(row - 1, col))
  {
    visit[row - 1][col] = true;
    solve_recursive(row - 1, col);
  }
}
void solve() {
  for (int i = 0; i < n; ++i) {
    for (int j = 0; j < n; ++j) {
      if (fit_condition(i, j)) {
        visit[i][j] = true;
        solve_recursive(i, j);
        group.push_back(my_counter);
        my_counter = 0;
      }
    }
  }
  sort(group.begin(), group.end());
}
void solve_queue() {
  queue<pair<int, int>> q;
  int c = 0;
  for (int i = 0; i < n; ++i) {
    for (int j = 0; j < n; ++j) {
      if (fit_condition(i, j)) {
        q.push(make_pair(i, j));
        visit[i][j] = true;
        while (!q.empty()) {
          int row = q.front().first;
          int col = q.front().second;
          ++c;
          q.pop();
          if (fit_condition(row, col + 1)) {
            q.push(make_pair(row, col + 1));
            visit[row][col + 1] = true;
          }
          if (fit_condition(row, col - 1)) {
            q.push(make_pair(row, col - 1));
            visit[row][col - 1] = true;
          }
          if (fit_condition(row + 1, col)) {
            q.push(make_pair(row + 1, col));
            visit[row + 1][col] = true;
          }
          if (fit_condition(row - 1, col)) {
            q.push(make_pair(row - 1, col));
            visit[row - 1][col] = true;
          }
        }
        group.push_back(c);
        c = 0;
      }
    }
  }
  sort(group.begin(), group.end());
}
void solve_stack() {
  stack<pair<int , int>> s;
  int c = 0;
  for (int i = 0; i < n; ++i) {
    for (int j = 0; j < n; ++j) {
      if (fit_condition(i, j)) {
        s.push(make_pair(i, j));
        visit[i][j] = true;
        while (!s.empty()) {
          int row = s.top().first;
          int col = s.top().second;
          ++c;
          s.pop();
          if (fit_condition(row, col + 1)) {
            s.push(make_pair(row, col + 1));
            visit[row][col + 1] = true;
          }
          if (fit_condition(row, col - 1)) {
            s.push(make_pair(row, col - 1));
            visit[row][col - 1] = true;
          }
          if (fit_condition(row + 1, col)) {
            s.push(make_pair(row + 1, col));
            visit[row + 1][col] = true;
          }
          if (fit_condition(row - 1, col)) {
            s.push(make_pair(row - 1, col));
            visit[row - 1][col] = true;
          }
        }
        group.push_back(c);
        c = 0;
      }
    }
  }
  sort(group.begin(), group.end());
}
int main() {
  input();
  // for_each(map.begin(), map.end(), [](string str) {cout << str << endl;});
  solve();
  cout << group.size() << endl;
  for_each(group.begin(), group.end(), 
    [](int x){cout << x << endl;});
}
