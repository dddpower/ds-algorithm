#include <iostream>
#include <map>
#include <algorithm>
using namespace std;
int main() {
    ios_base::sync_with_stdio(false);
    int x;
    map<int, int> my_map;
    cin >> x;
    const int length = x;
    for (int i = 0; i < length; ++i) {
        cin >> x;
        my_map[x]++;
    }
    for (auto it = my_map.begin(); it != my_map.end(); ++it) {
        for (int i = 0; i < it->second; ++i) {
            cout << it->first << '\n';
        }
    }
}
