#include <bits/stdc++.h>
using namespace std;

int c = 0;
vector<vector<int>> edge;
vector<bool> visit_table;
int num_v;
int num_e;
void dfs(int v) {
  visit_table[v] = true;
  ++c;
  for (auto it = edge[v].begin(); it != edge[v].end(); ++it) {
    if (!visit_table[*it])
      dfs(*it);
  }
}
void input() {
  cin >> num_v >> num_e;
  edge.resize(num_v + 1);
  visit_table.resize(num_v + 1);
  for (int i = 0; i < num_e; ++i) {
    int x, y;
    cin >> x >> y;
    edge[x].push_back(y);
    edge[y].push_back(x);
  }
}
int main() {
  input();
  dfs(1);
  cout << c - 1 << endl;
}
