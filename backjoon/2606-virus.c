#include <stdio.h>
#include <stdbool.h>
#define MAX_NUM 100

bool graph[MAX_NUM + 1][MAX_NUM + 1];
bool visit_table[MAX_NUM + 1];
int num_vertex;
int num_edge;
int count = 0;
void input() {
  scanf("%d", &num_vertex);
  scanf("%d", &num_edge);
  for (int i = 0; i < num_edge; ++i) {
    int x, y;
    scanf("%d %d", &x, &y);
    graph[x][y] = true;
    graph[y][x] = true;
  }
}
void visit(int vertex) {
  // start from 1
  visit_table[vertex] = true;
  count++;
  for (int i = 1; i <= num_vertex; ++i) {
    if (graph[vertex][i] && !visit_table[i]) {
      visit(i);
    }
  }
}
int main() {
  input();
  visit(1);
  printf("%d\n", count - 1);
}
