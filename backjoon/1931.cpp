#include <bits/stdc++.h>
#include <iostream>
using namespace std;
using dtype = vector<pair<size_t, size_t>>;
multimap<size_t, pair<size_t, size_t>> inputs{};
size_t n;
size_t c{0};
void print_inputs() {
    cout << "print" << endl;
    for (const auto& val : inputs) {
        cout << val.first << " " << val.second.first << " " << val.second.second << endl;
    }
}
void input() {
    cin >> n;
    size_t start, end;
    for (size_t i = 0; i < n; ++i) {
        cin >> start >> end;
        if (start == end)
          ++c;
        else {
          inputs.insert(make_pair(end - start, make_pair(start, end)));
        }
    }
}
auto solve() -> size_t {
    while (size(inputs) > 0) {
        ++c;
        auto start_time = begin(inputs)->second.first;
        auto end_time = begin(inputs)->second.second;
        inputs.erase(begin(inputs));
        auto it = begin(inputs);
        while (it != end(inputs)) {
            if (it->second.first <= end_time && it->second.second >= start_time) {
                it = inputs.erase(it);
            } else {
                ++it;
            }
        }
    }
    return c;
}

int main() {
    input();
    auto result = solve();
    cout << result << endl;
}

