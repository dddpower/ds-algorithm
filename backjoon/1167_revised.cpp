#include <bits/stdc++.h>
#include <cstdio>
using namespace std;
using graph = map<int, map<int, int>>;
graph g{};
void input() {
    int a, b, c, n;
    cin >>n;
    for (int i = 0; i < n; ++i) {
        cin >> a;
        while (true) {
            cin >> b;
            if (b == -1) {
                break;
            }
            cin >> c;
            g[a][b] = c;
        }
    }
}

auto xx = []() -> int {
    return 0;
};

pair<int, int> far_bfs(int from) {
    // data structures
    using dist = vector<int>;
    using visit = vector<bool>;
    dist d{};
    visit visited{};
    d.resize(g.size() + 1);
    d.shrink_to_fit();
    visited.resize(g.size() + 1);
    visited.shrink_to_fit();
    queue<int> to_visit;

    // helper functions
    auto q_top_pop_visit = [&visited, &to_visit]() {
        int val = to_visit.front();
        assert(visited[val] == false);
        visited[val] = true;
        to_visit.pop();
        return val;
    };
    auto not_visit_then_push = [&visited, &to_visit](int n) -> void {
        if(!visited[n]) {
            to_visit.push(n);
        }
    };

    // algorithm start
    // preprocess
    not_visit_then_push(from);
    // bfs
    while (!to_visit.empty()) {
        int n = q_top_pop_visit();
        for (auto adj_v : g[n]) {
            const auto& to = adj_v.first;
            const auto& len = adj_v.second;
            if (!visited[to]) {
                d[to] = d[n] + len;
                to_visit.push(to);
            }
        }
    }
    int max = 0;
    int n;
    for (int i = 1; i < d.size(); ++i) {
        // printf("d[%d] = %d\n", i, d[i]);
        if (max < d[i]) {
            max = d[i];
            n = i;
        }
    }
    return make_pair(n, max);
}

int main() {
    input();
    auto result = far_bfs(far_bfs(1).first);
    cout << result.second << endl;
    // cout << "fartest vertex = " << result.first << endl;
    // cout << "and distance = " << result.second << endl;
}
