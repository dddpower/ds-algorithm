class queue {
  int front, rear;
  int capacity;
  int* memory;
  public:
  bool isFull();
  bool isEmpty();
  bool push(int data);
  int pop();
  queue(int capacity = 1) {
    memory = new int[capacity];
  }
  ~queue() {
    delete[] memory;
  }
};
