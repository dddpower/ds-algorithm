#include <bits/stdc++.h>
using namespace std;

// '.', "#", 'v', 'o'
int row, col;
vector<string> my_map;
vector<vector<bool>> visit;
void input() {
    cin >> row >> col;
    my_map.resize(row);
    visit.resize(row);
    for (auto i  = visit.begin(); i != visit.end(); ++i) {
        i->resize(col);
    }
    for (int i = 0; i < row; ++i) {
        cin >> my_map[i];
    }
    for (auto i = my_map.begin(); i < my_map.end(); ++i) {
        cout << *i << endl;
    }
}
bool inbound(int a, int b) {
    return a >= 0 && a < row && b >= 0 && b < col;
}
bool isWall(int row, int col) {
    return my_map[row][col] == '#';
}
bool visitable(int a, int b) {
    return inbound(a, b) && !isWall(a, b) && !visit[a][b];
}
int numV = 0;
int numO = 0;
#define CAN_VISIT_THEN_VISIT(row, col)  \
    if (visitable(row, col)) dfs(row, col, v, o);
pair<int, int> dfs(int row, int col, int v, int o) {
    visit[row][col] = true;
    if (my_map[row][col] == 'v') {
        ++v;
    }
    if (my_map[row][col] == 'o') {
        ++o;
    }
    CAN_VISIT_THEN_VISIT(row, col + 1)
    CAN_VISIT_THEN_VISIT(row, col - 1)
    CAN_VISIT_THEN_VISIT(row + 1, col)
    CAN_VISIT_THEN_VISIT(row - 1, col)
    return make_pair(v, o);
}
void solve() {
    int v = 0;
    int o = 0;
    for (int i = 0; i < row; ++i) {
        for (int j = 0; j < col; ++j) {
            if (visitable(i, j)) {
                auto result = dfs(i, j, 0, 0);
                if (result.first >= result.second) {
                    v += result.first;
                } else {
                    o += result.second;
                }
            }
        }
    }
    cout << v << " " << o << endl;
}
int main() {
    input();
    solve();
}
