#include <stdio.h>
#define SIZE 500
int m[SIZE][SIZE];
int result[SIZE][SIZE];

int T;
int N;  // 1<= N <= 500 홀수
int D; // |D| <= 360, 45 배수

typedef struct _point {
  int row, col;
} point;

// casenum >= 0 && casenum <= 7
point next(point p, int casenum) {
  switch (casenum) {
    default:
      printf("exception! casenum = %d\n", casenum);
      break;
    case 0:
      ++p.row;
      ++p.col;
      break;
    case 1:
      ++p.row;
      break;
    case 2:
      ++p.row;
      --p.col;
      break;
    case 3:
      --p.col;
      break;
    case 4:
      --p.row;
      --p.col;
      break;
    case 5:
      --p.row;
      break;
    case 6:
      --p.row;
      ++p.col;
      break;
    case 7:
      ++p.col;
      break;
  }
  return p;
}

point static_point[8];
void decide_8p() {
  point p;
  int min = 0;
  int max = N - 1;
  int half = N / 2;

  p.row = min;
  p.col = min;
  static_point[0] = p;

  p.row = min;
  p.col = half;
  static_point[1] = p;

  p.row = min;
  p.col = max;
  static_point[2] = p;

  p.row = half;
  p.col = max;
  static_point[3] = p;

  p.row = max;
  p.col = max;
  static_point[4] = p;

  p.row = max;
  p.col = half;
  static_point[5] = p;

  p.row = max;
  p.col = min;
  static_point[6] = p;

  p.row = half;
  p.col = min;
  static_point[7] = p;
}
void write_line(int p_i, int count) {
  int p_j = (p_i + count) % 8;
  point p1 = static_point[p_i];
  point p2 = static_point[p_j];
  for (int i = 0; i < N; ++i) {
    result[p2.row][p2.col] = m[p1.row][p1.col];
    p1 = next(p1, p_i);
    p2 = next(p2, p_j);
  }
}
int calculate_count() {
  if (!D) {
    return 0;
  }
  int num = D / 45;
  return num > 0 ? num : 8 + num;
}
void solve() {
  decide_8p();
  int count = calculate_count();
  for (int i = 0; i < 4; ++i) {
    write_line(i, count);
  }
}

int main() {
  scanf("%d", &T);
  for (int t = 0; t < T; ++t) {
    scanf("%d %d", &N, &D);
    for (int i = 0; i < N; ++i) {
      for (int j = 0; j < N; ++j) {
        scanf("%d", &m[i][j]);
        result[i][j] = m[i][j];
      }
    }

    solve();

    // printf result
    for (int i = 0; i < N; ++i) {
      for (int j = 0; j < N; ++j) {
        printf("%d ", result[i][j]);
      }
      printf("\n");
    }
  }
}
