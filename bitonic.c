#include <stdio.h>
#define NUM 1001
int N;
int in[NUM];
int reverse[NUM];
void input() {
  scanf("%d", &N);
  for (int i = 1; i <= N; ++i) {
    int val;
    scanf("%d", &val);
    in[i] = val;
    reverse[N + 1 - i] = val;
  }
}
int mem_inc[NUM];
int mem_dec[NUM];
void bitonic_base(int rec[], int src[]) {
  for (int i = 1; i <= N; ++i) {
    int max = 0;
    for (int j = 1; j < i; ++j) {
      int val = rec[j];
      if (max < val && src[j] < src[i]) {
        max = val;
      }
    }
    rec[i] = max + 1;
  }
}

// assemble mem_inc & mem_dec
int bitonic() {
  bitonic_base(mem_inc, in);
  bitonic_base(mem_dec, reverse);
  int max = 0;
  for (int i = 1; i <= N; ++i) {
    int val = mem_inc[i] + mem_dec[N - i + 1];
    max = max > val ? max : val;
  }
  return max - 1;
}
int main() {
  input();
  int ans = bitonic();
  printf("%d\n", ans);
}
