#include <stdio.h>
#include <assert.h>
#define MAXN 101
#define MAXNUM 1000000000
int mem[MAXN][10];
int sum(int n) {
  // sum of mem[n][]
  int sum = 0;
  for (int i = 0; i <= 9; ++i) {
    sum += mem[n][i];
  }
  return sum;
}
void print_mem(int n) {
  for (int i = 0; i < 10; ++i) {
    printf("%d ", mem[n][i]);
  }
  printf("\n");
}
unsigned long long easy_stair_number(int n) {
  // mem[0] ~ mem[1][0] = 1
  for (int i = 1; i <= 9; ++i) {
    mem[1][i] = 1;
    // printf("%daa",mem[1][i]);
  }
  // assert(mem[1][1] == 1);
  for (int i = 2; i <= n; ++i) {
    for (int j = 0; j <= 9; ++j) {
      unsigned long long val1 = j >= 1 ? mem[i - 1][j - 1] : 0;
      unsigned long long val2 = j < 9 ? mem[i - 1][j + 1] : 0;
      mem[i][j] = (val1 + val2) % MAXNUM;
    }
  }
  return sum(n);
}
int main() {
  int n;
  scanf("%d", &n);
  unsigned long long ans = easy_stair_number(n);
  printf("%llu\n", ans);
  // print_mem(1);
}
