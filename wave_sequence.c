#include <stdio.h>
#define MAX 1001
int T;
int N;
unsigned long long int mem[MAX];

void wave_sequence() {
  mem[1] = 1;
  mem[2] = 1;
  mem[3] = 1;
  mem[4] = 2;
  mem[5] = 2;
  for (int i = 6; i <= MAX - 1; ++i)
    mem[i] = mem[i - 1] + mem[i - 5];
}
int main() {
  wave_sequence();
  scanf("%d", &T);
  for (int i = 0; i < T; ++i) {
    scanf("%d", &N);
    printf("%lld\n", mem[N]);
  }
}
