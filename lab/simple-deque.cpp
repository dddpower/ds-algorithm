#include <list>
class simple_deque {
    private:
        std::list<int> data;
    public:
        void push_back (int x) {
            data.emplace_back(x);
        }
        void push_front (int x) {
            data.emplace_front(x);
        }
        int pop_back (int x) {
            int val = *data.rbegin();
            data.pop_back();
            return val;
        }
};
