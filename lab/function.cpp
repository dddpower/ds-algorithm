int foo(int a) {
    return a + 1;
}
int main() {
    auto a = foo;
    decltype(a) b = a;
}
