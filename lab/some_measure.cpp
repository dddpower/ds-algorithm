#include <iostream>
#include <chrono>
#include <array>
#include <algorithm>

enum {big_num = 1000000000};
// using chunk = std::array<int , big_num>;
// auto var1 = chunk();
int64_t func1(int64_t a) {
    return a;
}
std::array<int, 10> a{};
std::array<int, 10> func2() {
    return std::array<int, 10>{};
}
int main() {
    auto test = func2();
    std::for_each(test.begin(), test.end(), [](auto x) {std::cout << x << " ";});
    // auto start = std::chrono::system_clock::now();
    // int64_t a = 10;
    // for (int64_t i = 0; i < big_num; ++i) {
    //     int64_t x = func1(a);
    // }
    // auto end = std::chrono::system_clock::now();
    // std::cout << "elapsed time : " << std::chrono::duration<double>{end - start}.count() << std::endl;
}
