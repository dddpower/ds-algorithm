#include <iostream>
#include <memory>
using namespace std;
class Class {
    public:
        void call_by_ref (Class& c) {
            cout << this << '\n';
            cout << &c << '\n';
        }
};


int main() {
    Class A;
    A.call_by_ref(A);
}
