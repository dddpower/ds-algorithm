#include <iostream>
int value = 0;
int construct_counter = 0;
int copy_construct_counter = 0;
int copy_assignment_operator = 0;
class foo {
    static int value;
    public:
    int inner_counter;
    foo() {
        ++construct_counter;
    }
    foo(const foo& a) {
        ++copy_construct_counter;
    }
    foo& operator = (const foo& a) {
        ++copy_assignment_operator;
        return *this;
    }
};
void ref_test() {
    // foo a = foo();
    // return a;
    foo();
    foo a;
    foo b(a);
    foo c = b;
}
int main() {
    // foo b = ref_test();
    ref_test();
    std::cout << "foo was constructed " << construct_counter << " times\n";
    std::cout << "foo was copy constructed " << copy_construct_counter << " times\n";
    std::cout << "copy assignment op was called " << copy_assignment_operator << " times\n";
}
