#include <iostream>

template <typename T>
class Auto_ptr4 {
  T* m_ptr;
  public:
  Auto_ptr4(T* ptr = nullptr)
  :m_ptr(ptr) {
    
  }
  ~Auto_ptr4() {
    delete m_ptr;
  }

  // Copy constructor which executes deep copy.
  Auto_ptr4(const Auto_ptr4& a) {
    m_ptr = new T;
    *m_ptr = *a.m_ptr;
  }

  // Move constructor, which transfers ownership of pointer
  Auto_ptr4(Auto_ptr4&& a) : m_ptr(a.m_ptr) {
    a.m_ptr = nullptr;
  }

  //Copy assignment (deep copy)
  Auto_ptr4& operator=(const Auto_ptr4& a) {
    if (&a == this)
      return *this;
    
    // Relesase any resource we're holding
    delete m_ptr;

    m_ptr = new T;
    *m_ptr = *a.m_ptr;

    return *this;
  }
  
  // Move assignment
  // Transfer ownership of a.m_ptr to m_ptr
  Auto_ptr4& operator=(Auto_ptr4&& a) {
    if (&a == this)
      return *this;
    
    delete m_ptr;
    m_ptr = a.m_ptr;
    a.m_ptr = nullptr;

    return *this;
  }
  T& operator*() const {return *m_ptr;}
  T* operator->() const {return m_ptr;}
  bool isNull() const { return m_ptr == nullptr;}
};

class Resource {
  public:
  Resource() { std::cout << "Resource acquired\n";}
  ~Resource() { std::cout << "Resource destroyed\n";}
};

Auto_ptr4<Resource> generateResource() {
  Auto_ptr4<Resource> res(new Resource);
  return res; // this return value will invoke the move constructor
}

int main() {
  Auto_ptr4<Resource> mainres;
  mainres = generateResource();

  return 0;
}