#include <iostream>
#include <vector>
using std::vector;
using std::cout;
using std::endl;

int main() {
    vector<bool> test(4, false);
    vector<int> test2(4, 0);
    for (auto t : test) {
        t = true;
    }
    for (auto t : test) {
        cout << t << endl;
    }

    for (auto t : test2) {
        t = true;
    }
    for (auto t : test2) {
        cout << t << endl;
    }
}
