#include <iostream>
using namespace std;

int main() {
    int value{5};
    int a = 4;
    int& ref{value};
    ref += 1;
    cout << value << endl;
    cout << ref << endl;
    ref = a;
    cout << &ref << endl;
    cout << ref << endl;
    cout << value << endl;
}
