#include <iostream>
using namespace std;
template<typename T>
class Auto_ptr {
    T* m_ptr;
    public:
    Auto_ptr(T* ptr) : m_ptr(ptr){}
    Auto_ptr(Auto_ptr& ptr) {
        m_ptr = ptr.m_ptr;
        ptr.m_ptr = nullptr;
    }
    Auto_ptr& operator = (Auto_ptr& ptr) {
        if (this == &ptr) {
            return *this;
        }
        delete m_ptr;
        m_ptr = ptr.m_ptr;
        ptr.m_ptr = nullptr;
        return *this;
    }
    ~Auto_ptr() {
        delete m_ptr;
    }
    T& operator *() const {
        return *m_ptr;
    }
    T* operator ->() const {
        return m_ptr;
    }
};
class Resource {
    public:
        void say_hi() const {
            cout << "Hi\n";
        }
        Resource() {
            cout << "Resource acquired\n";
        }
        ~Resource() {
            cout << "Resource destroyed\n";
        }
};
template <typename T>
void passByValue(Auto_ptr<T> ptr) {
    //first problem of auto_ptr
    cout << "enter passByValue\n";
}
// void passByValue(Auto_ptr<Resource> ptr) {
// 
// }
int main() {
    Auto_ptr<Resource> ptr1{new Resource};
    passByValue(ptr1);
    ptr1->say_hi();
}
