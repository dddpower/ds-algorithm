#include <iostream>
int main() {
  const char* x = "Hello world";
  // const char& y = "hello world";
  const int& y = 4;
  const char& z = "hello";
  // char&& z = "hello";
  std::cout << y << std::endl;
  std::cout << z << std::endl;
}
