#include <stdio.h>
#include <stdlib.h>

enum {magic_number = 1000000};
 

void a()
{
   int i;
   for (i = 0; i < magic_number; i++) {
        printf("Func A = %d\n", i / 7);
   }
}

 

void b()
{
   int i;
   srand(3);
   for (i = 0; i < magic_number; i++) {
        printf("Func B = %d\n", rand()%10000);
   }
}

 

void c2()
{
   int i;
   for (i = 0; i < magic_number; i++) {
        printf("Func C2 = %d\n", i);
   }
}

 

void c()
{
   int i;
   for (i = 0; i < 1000; i++) {
        printf("Func C = %d\n", i);
        c2();
   }
}

 

int main(int argc, char **argv)
{
   a();  //1000000 만큼 나누기 연산을 수행
   b();  //1000000 만큼 rand() 호출
   c();  //1000000 만큼 단순한 인덱스 출력

 

   return 0;
}
