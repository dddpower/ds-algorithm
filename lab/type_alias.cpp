#include <array>

int main() {
    // using을 사용하면 표현이 더 간결해지고, 템플릿의 type_alias가 가능하다는 
    // 이점이 있다.
    using index_t = std::array<int, 5>::size_type;
    typedef std::array<int, 5>::size_type index_type;
}
