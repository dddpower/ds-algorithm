#include <bits/stdc++.h>
using namespace std;
int num(char ch) {
  return ch - '0';
}
char ch(int n) {
  return n + '0';
}
string solve(int k) {
  queue<string> q{};
  q.push("1");
  q.push("2");
  q.push("3");
  q.push("4");
  q.push("5");
  q.push("6");
  q.push("7");
  q.push("8");
  q.push("9");
  
  string val{""};
  while (k--) {
    val = q.front();
    q.pop();
    int last_num = num(*(val.end() - 1));
    if (last_num == 0) {
      q.push(val.append("1"));
    } else if (last_num == 9) {
      q.push(val.append("8"));
    } else {
      q.push(val + ch(last_num - 1));
      q.push(val + ch(last_num + 1));

    }

  }
  return val;

}
int main() {
  int k;
  cin >> k;
  string val = solve(k);
  cout << val << endl;
}
