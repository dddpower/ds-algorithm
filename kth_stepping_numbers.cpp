#define LEETCODE
#ifdef LEETCODE
#include <bits/stdc++.h>
using namespace std;
#endif


vector<vector<int>> mem{};

int width(int d, int x) {
  if (x < 0 || x > 9)
    return 0;
  
  if (d == 1) {
    if (x == 0)
      return 0;
    return 1;
  }

  if (mem.size() < d + 1)
    mem.resize(mem.size() * 2);
  if (mem[d].size() < x + 1)
    mem[d].resize(mem[d].size() * 2);

  if (!mem[d][x])
    mem[d][x] = width(d - 1, x - 1) + width(d - 1, x + 1);

  return mem[d][x];
}
int width(int d) {
  int sum;
  for (int i = 0; i <= 9; ++i) {
    sum += width(d, i);
  }
  return sum;
}

string solve(string acc, int root, int d, int w) {
  if (!d)
    return acc;
  if (root == 0) {
    return solve(acc + "1", 1, d - 1, w);
  } else if (root == 9) {
    return solve(acc + "8", 8, d - 1, w);
  } else {
    int val = width(d - 1, root - 1);
    if (w > val) {
      acc += root + 1 + '0';
      return solve(acc, root + 1, d - 1, w - val);
    }
    acc += root - 1 + '0';
    return solve(acc, root - 1, d - 1, w);
  }
}
#ifdef LEETCODE
int main() {
  int k;
  cin >> k;
  int root, d, w;
  int i = 0;
  // while (k >= 0) {
  //   k -= width(++i);
  // }
  // auto result = solve("", r, d, w);
  // cout << result;
}
#endif
