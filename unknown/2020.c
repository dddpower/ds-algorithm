#include <cstdio>
#include <algorithm>
#include <cassert>
#define INPUT_INT(X) scanf("%d", &X)
#define PRINT_INT(X) scanf("%d\n", X)

int candy, chocolate;
int array1[] = {1,1,1,1,2,2};
int len1 = 6;
int array2[] = {1,1,1,1,1,2,2};
int len2 = 7;
int array3[] = {1,1,1,2,2};
int len3 = 5;
void input() {
}

int solve(int arr[], int len) {
  int count = 0;
  do {
    ++count;
  } while (std::next_permutation(arr, arr + len));
  return count;
}

int main () {
  int val1 = solve(array1, len1);
  int val2 = solve(array2, len2);
  int val3 = solve(array3, len3);
  int ans = val1 * val1 + val2 * val3;
  printf("%d\n", ans);
  return 0;
}
