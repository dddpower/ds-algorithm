#include <stdio.h>
#include <stdbool.h>
#define MAX_N 20
#define MAX_VAL 30
#define INPUT_INT(X) scanf("%d", &X);
#define PRINT_INT(X) printf("%d", X);
#define PRINTLN printf("\n");
int n;
int in[MAX_N + 1];
// -1 : unvisit
int mem[MAX_N * MAX_VAL / 3 + 1][MAX_N * MAX_VAL / 3 + 1][MAX_N + 1];
int sum;
void init_mem() {
  for (int i = 0; i <= sum / 3; ++i) {
    for (int j = 0; j <= sum / 3; ++j) {
      for (int k = 1; k <= n; ++k) {
        mem[i][j][k] = -1;
      }
    }
  }
}
bool partition(int s1, int s2, int n) {
  if (s1 < 0)
    return false;
  if (s2 < 0)
    return false;
  if (!s1 && !s2)
    return true;
  if (!n)
    return false;
  if (mem[s1][s2][n] == -1)
    mem[s1][s2][n] = partition(s1, s2, n - 1) ||
          partition(s1 - in[n], s2, n - 1) ||
          partition(s1, s2 - in[n], n - 1);
  return mem[s1][s2][n];
}
bool solve_partition() {
  init_mem();
  if (sum % 3)
    return false;
  return partition(sum / 3, sum / 3, n);
}
int main() {
  sum = 0;
  INPUT_INT(n)
  for (int i = 1; i <= n; ++i) {
    INPUT_INT(in[i])
    sum += in[i];
  }
  bool ans = solve_partition();
  printf("%d\n", ans);
}
