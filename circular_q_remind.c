#include <stdio.h>
#include <stdbool.h>
#define MAX 7
int queue[MAX];
int front = 0;
int rear = 0;
bool isEmpty() {
  return front == rear;
}
bool isFull() {
  return (rear + 1) % MAX == front;
}
bool enqueue(int val) {
  if (!isFull()) {
    queue[rear++] = val;
    rear %= MAX;
    return true;
  }
  return false;
}
int dequeue() {
  if (!isEmpty()) {
    int val = queue[front++]; 
    front %= MAX;
    return val;
  }
  return -1;
}
int main() {
  enqueue(1);
  enqueue(2);
  enqueue(3);
  enqueue(4);
  enqueue(5);
  enqueue(6);
  printf("%d ",dequeue());
  printf("%d ",dequeue());
  printf("%d ",dequeue());
  enqueue(8);
  enqueue(9);
  enqueue(1);
  printf("%d ",dequeue());
  printf("%d ",dequeue());
  printf("%d ",dequeue());
  printf("%d ",dequeue());
  printf("%d ",dequeue());
  printf("%d ",dequeue());
  printf("\n");
}
