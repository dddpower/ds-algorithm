#include <stdio.h>
#include <string.h>
#define MAX 10000
char in[MAX];
void input() {
  scanf("%s", in);
}
// consecutive removal solution
int solve_1() {
  int len = strlen(in);
  char c = 0, p;
  int accumal = 0;
  int sum_acc = 0;
  for (int i = 0; i < len; ++i) {
    p = c;
    c = in[i];
    // consecutive
    if (c == p) {
      ++accumal;
    } else {
      sum_acc += accumal;
      accumal = 0;
    }
  }
  sum_acc += accumal;
  return sum_acc;
}
// consecutive replace solution
int solve_2() {
  int len = strlen(in);
  int c = 0, p;
  int sum = 0;
  for (int i = 0; i < len; ++i) {
    p = c;
    c = in[i];
    if (p == c) {
      ++sum;
      c = 0;
    }
  }
  return sum;
}
int main() {
  input();
  int ans1 = solve_1();
  int ans2 = solve_2();
  printf("%d %d\n", ans1, ans2);
}
