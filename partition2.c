#include <stdio.h>
#include <stdbool.h>
#define MAX_T 100
#define MAX_N 100
#define MAX_VAL 1000
#define INPUT_INT(X) scanf("%d", &X);
#define PRINT_INT(X) printf("%d", X);
#define PRINTLN printf("\n");
int t, n;
int in[MAX_N + 1];
// -1 : unvisit
int mem[MAX_N * MAX_VAL + 1][MAX_N + 1];
int sum;
void init_mem() {
  for (int i = 1; i <= sum; ++i) {
    for (int j = 1; j <= n; ++j) {
      mem[i][j] = -1;
    }
  }
}
bool partition(int s, int n) {
  if (!s) 
    return true;
  if (!n || s < 0)
    return false;
  // s > 0 && n > 0
  if (mem[s][n] == -1)
    mem[s][n] = partition(s, n - 1) || partition(s - in[n], n - 1);
  return mem[s][n];
}
bool solve_partition() {
  init_mem();
  if (sum % 2)
    return false;
  return partition(sum / 2, n);
}
int main() {
  INPUT_INT(t)
  for (int j = 0; j < t; ++j) {
    sum = 0;
    INPUT_INT(n)
      for (int i = 1; i <= n; ++i) {
        INPUT_INT(in[i])
          sum += in[i];
      }
    bool ans = solve_partition();
    if (ans) {
      printf("YES");
    } else {
      printf("NO");
    }
    PRINTLN
  }
}
