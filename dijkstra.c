#include <assert.h>
#include <stdio.h>
#include <stdbool.h>
#include <limits.h>
#include <stdlib.h>
#define MAX_V 20001
#define MAX_E 300001
#define INF INT_MAX
#define PRINT_INT(X) printf("%d", X)
#define PRINTLN printf("\n")
#define INPUT_INT(X) scanf("%d", &X)
#define MAX 1000
typedef struct _pair {
  int first;
  int second;
} pair ;
pair Pair(int first, int second) {
  pair p;
  p.first = first;
  p.second = second;
  return p;
}
typedef struct _node node;
struct _node {
  pair p;
  node* next;
};
node adj_list_head[MAX_V];
node sp_list_head;
node graph[MAX_V];

void adj_insert(int v1, pair p) {
  // insert to the head
  node* temp = malloc(sizeof(node));
  *temp = adj_list_head[v1];
  adj_list_head[v1].p = p;
  adj_list_head[v1].next = temp;
}
void spt_insert(pair p) {
  node* temp = malloc(sizeof(node));
  *temp = sp_list_head;
  sp_list_head.p = p;
  sp_list_head.next = temp;
}
// first: vertex, second: dist
pair heap[MAX];
int top = -1;
bool isFull() {
  return top == MAX - 1;
}
bool isEmpty() {
  return top == -1;
}
int parent_i(int i) {
  if (i)
    return (i + 1) / 2 - 1;
  // root
  return -1;
}
int l_child_i(int i) {
  return 2 * i + 1;
}
//right_child_i = left_child_i + 1

void swap_pair(pair *a, pair *b) {
  pair temp = *a;
  *a = *b;
  *b = temp;
}
bool heap_insert(pair val) {
  if (isFull())
    return false;
  heap[++top] = val;
  int i = top;
  while (i > 0) {
    int p_i = parent_i(i);
    if (heap[p_i].second > heap[i].second) {
      swap_pair(&heap[p_i], &heap[i]);
      i = p_i;
    } else {
      break;
    }
  }
  return true;
}

pair pop() {
  if (isEmpty()) {
    assert("heap is empty and you tried pop()\n");
    pair p;
    return p;
  }
  int i = 0;
  int l_i = 1;
  int r_i = 2;
  pair return_val = heap[i];
  while (l_i < top) {
    if (heap[l_i].second < heap[r_i].second) {
      swap_pair(&heap[i], &heap[l_i]);
      i = l_i;
    } else {
      swap_pair(&heap[i], &heap[r_i]);
      i = r_i;
    }
    l_i = l_child_i(i);
    r_i = l_i + 1;
  }
  heap[i] = heap[top--];
  return return_val;
}

void print_heap() {
  int newlinecount = 0;
  int twos_multiply = 1;
  for (int i = 0; i <= top; ++i) {
    printf("(%d %d)", heap[i].first, heap[i].second);
    ++newlinecount;
    if (newlinecount == twos_multiply) {
      printf("\n");
      twos_multiply *= 2;
      newlinecount = 0;
    }
  }
  printf("\n");
}

int dist[MAX_V];
bool spt[MAX_V];
node* spt_list;
int V, E, start;
int u, v, w;
void input() {
  INPUT_INT(V);
  INPUT_INT(E);
  INPUT_INT(start);
  for (int i = 0; i < E; ++i) {
    INPUT_INT(u);
    INPUT_INT(v);
    INPUT_INT(w);
    pair p = Pair(v, w);
    adj_insert(u, p);
  }
}

void dijkstra() {
  for (int i = 1; i <= V; ++i) {
    dist[i] = INF;
  }
  dist[start] = 0;
  int spt_count = V;
  heap_insert(Pair(start, 0));
  while (spt_count) {
    // Pop from heap, and it is min dist on the current iteration.
    pair p = pop();
    spt_insert(p);
    // update dist[adj p], push updated dists to heap.
    int min_dist_v = p.first;
    node adj_vertex = adj_list_head[p.first];
    while (adj_vertex.next) {
      int vn = adj_vertex.p.first;
      dist[vn] = 
        dist[vn] > dist[min_dist_v] + adj_list_head[min_dist_v][n.p.first] ? 
    }
    int min_i = min_dist.first;
    --spt_count;
    node n = adj_list_head[min_i];
    while (n.next) {
      
    }
  }
}


void print_dist() {
  for (int i = 1; i <= V; ++i) {
    int val = dist[i];
    if (val != INF)
      PRINT_INT(dist[i]);
    else
      printf("INF");
    PRINTLN;
  }
}

int main() {
  input();
  // print_adjl();
  dijkstra();
  print_dist();
}
