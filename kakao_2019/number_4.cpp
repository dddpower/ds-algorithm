#include <assert.h>
#include <bits/stdc++.h>
using namespace std;




int Solve(vector<int> source, long long k) {
  list<pair<int, int>> l;
  for (size_t i = 0; i < source.size(); ++i) {
    l.push_back(pair<int, int> {i, source[i]});
  }
  auto it = l.begin();
  for (int i = 0; i < k; ++i) {
    int val = --(*it).second;
    if (val) {
      ++it;
    } else {
      it = l.erase(it);
    }
  }
  return 0;
}
int main() {
  int ans = Solve();
}
