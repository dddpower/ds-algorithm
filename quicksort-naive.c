#include <stdio.h>
#define MAX_N 100000
int in[MAX_N];
int n;
void input() {
    scanf("%d", &n);
    for (int i = 0; i < n; ++i) {
        scanf("%u", &in[i]);
    }
}
void swap(int *a, int *b) {
  int temp = *a;
  *a = *b;
  *b = temp;
}
void swap_by_index(int i1, int i2) {
  return swap(&in[i1], &in[i2]);
}
void quicksort(int* src, int l, int r) {
  if (l >= r) {
    return;
  }
  // median of three
  int m = l + (r - l) / 2;
  if (src[l] < src[m]) {
    if (src[m] < src[r]) {
      swap_by_index(m, r);
    }
  } else {
    if (src[l] < src[r]) {
      swap_by_index(l, r);
    } else {
      swap_by_index(m, r);
    }
  }
  int p = src[r];
  int l_i = l;
  int l_p = l - 1;
  int r_i = r - 1;
  int r_p = r;
  while (1) {
    while (src[l_i] <= p) {
      if (src[l_i] == p) {
        swap_by_index(l_i, ++l_p);
      }
      ++l_i;
    }
    while (src[r_i] >= p) {
      if (src[r_i] == p) {
        swap_by_index(r_i, --r_p);
      }
      --r_i;
    }
    if (l_i >= r_i) {
      break;
    }
    swap_by_index(l_i, r_i);
  }
  // gather pivots to center
  if (l_i > r_i) {
    --l_i, ++r_i;
  } else { // l_i = r_i;
    ++r_i;
  }
  while (l_p >= l) {
    swap_by_index(l_p--, l_i--);
  }
  while (r_p <= r) {
    swap_by_index(r_p++, r_i++);
  }
  quicksort(src, l, l_i);
  quicksort(src, r_i, r);
}
void print_array(int src[]) {
  for (int i = 0; i < n; ++i) {
    printf("%u ", src[i]);
  }
  printf("\n");
}
int main() {
  input();
  quicksort(in, 0, n - 1);
  print_array(in);
}
