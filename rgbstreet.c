#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <assert.h>
#define NUM 2000
#define MAGIC 3
const int red = 0, green = 1, blue = 2;
int RGBcost[NUM][MAGIC];
int cost_memory[NUM][MAGIC];
int min2(int a, int b) {
  return a < b ? a : b;
}
int min3(int a, int b, int c) {
  return min2(min2(a, b), c);
}
int exclusive_min(int index, int color) {
  int min = 2000000;
  for (int i = 0; i < MAGIC; ++i) {
    if (i != color) {
      int val = cost_memory[index][i];
      min = min < val ? min : val;
    }
  }
  return min;
}

int rgbstreet(int n) {
  for (int i = 1; i <= n; ++i) {
    for (int j = 0; j < MAGIC; ++j) {
      cost_memory[i][j] = exclusive_min(i - 1, j) + RGBcost[i][j];
    }
  }
  return min3(cost_memory[n][0], cost_memory[n][1], cost_memory[n][2]);
}
int main() {
  int N;
  scanf("%d", &N);
  for (int i = 1; i <= N; ++i) {
    scanf("%d %d %d", &RGBcost[i][0], &RGBcost[i][1], &RGBcost[i][2]);
  }
  int ans = rgbstreet(N);
  printf("%d\n", ans);
}
