#include <stdio.h>
#include <stdbool.h>
#define MAX_N 20
#define MAX_VAL 30
#define MAX_SUM MAX_N * MAX_VAL
#define INPUT_INT(X) scanf("%d", &X);
#define PRINT_INT(X) printf("%d", X);
#define PRINTLN printf("\n");
int n;
// map_verte[x] = number of vertexs which has x value
int map_vertex[MAX_VAL + 1];
const int bound = MAX_SUM / 3 + 1;
int actual_bound;
typedef enum {unvisit= -1, False, True} state;
state memory[bound];
void input() {
  INPUT_INT(n);
  for (int i = 1; i <= n; ++i) {
    memory[i] = unvisit;
  }
  int sum = 0;
  for (int i = 0; i < n; ++i) {
    int v;
    INPUT_INT(v);
    sum += v;
    ++map_vertex[v];
  }
  actual_bound = sum / 3;
}
bool possible(int x) {
  if (!x)
    return true;
  if (memory[x] == unvisit) {
    printf("memory[%d] = %d\n", x, memory[x]);
    for (int i = x; i > 0; --i) {
      if (map_vertex[i] && possible(x - i)) {
        printf("memory[%d] = true\n", x);
        return memory[x] = true;
      }
    }
    printf("memory[%d] = false\n", x);
    return memory[x] = false;
  } else {
    return memory[x];
  }
}
void print_memory() {
  for (int i = 1; i <= actual_bound; ++i) {
    PRINT_INT(memory[i])
    printf(" ");
  }
  PRINTLN
}
void print_map_vertex() {
  for (int i = 1; i <= MAX_VAL; ++i) {
    PRINT_INT(map_vertex[i]);
    printf(" ");
  }
  PRINTLN
}
int main() {
  input();
  possible(3);
  print_map_vertex();
  print_memory();
}
