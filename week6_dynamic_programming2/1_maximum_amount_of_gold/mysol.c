#include <stdio.h>
#define MAX_W 10000
#define MAX_N 301
#define INPUT_INT(X) scanf("%d", &X);
#define PRINT_INT(X) printf("%d", X);
#define PRINTLN printf("\n");
#define MAX(X, Y) X > Y ? X : Y;
int capacity, n;
int goldbar[MAX_N];
int memory[MAX_N][MAX_W];
void input() {
  INPUT_INT(capacity)
  INPUT_INT(n)
  for (int i = 1; i <= n; ++i) {
    INPUT_INT(goldbar[i])
  }
}

int solve() {
  for (int i = 1; i <= n; ++i) {
    for (int j = 1; j <= capacity; ++j) {
      if (j >= goldbar[i])
        memory[i][j] = MAX(memory[i - 1][j - goldbar[i]] + goldbar[i],
          memory[i - 1][j])
      else {
        memory[i][j] = memory[i - 1][j];
      }
    }
  }
  return memory[n][capacity];
}

int main() {
  input();
  int answer = solve();
  PRINT_INT(answer)
  PRINTLN
}
