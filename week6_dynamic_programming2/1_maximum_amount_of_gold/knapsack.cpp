#include <cstdio>
#define WMAX 10000
#define IMAX 300
#define MAX(a, b) a > b ? a : b
int W;
int N;
int wt[IMAX + 1];
int vt[IMAX + 1];
int mvt[WMAX + 1][IMAX + 1];

int Solve(int w, int n) {
  int* cv = &mvt[w][n];
  if (!*cv) {
    *cv = Solve(w - wt[n], n - 1) + vt[n], Solve()
  }

  return *cv;
}

int main(int argc, char** argv) {
  scanf("%d %d", &W, &N);
  for (int i = 1; i <= N; ++i) {
    scanf("%d", &wt[i]);
  }
  
  //init mvt
  for (int i = 0; i <= IMAX; ++i) {
    mvt[0][i] = 0;
  }
  for (int i = 0; i <= WMAX; ++i) {
    mvt[i][0] = 0;
  }

  // solve
  int answer = Solve();
  
  printf("%d\n", answer);
}

