#include <stdio.h>
#define WEIGHT 100000
#define VALUE 1000
#define ITEMNUM 100
#define MAX(a, b) a < b ? b : a
//int mvt[WEIGHT + 1][ITEMNUM + 1];
int value[ITEMNUM + 1];
int weight[ITEMNUM + 1];
int mvt[WEIGHT +1][ITEMNUM +1];
int mvt2[WEIGHT +1][ITEMNUM +1];
int N, W;

int Solve_dynamic();
int Solve_memoization(int w, int i);

int Solve(int w, int i);

int main() {
  int ans = -1;
  scanf("%d %d", &N, &W);
  for (int i = 1; i <= N; ++i) {
    scanf("%d %d", &weight[i], &value[i]);
  }
  
  printf("recursive solution\n");
  ans = Solve(W, N);
  printf("%d\n", ans);

  printf("\nmemoization solution\n");
  ans = Solve_memoization(W, N);
    for (int i = 1; i <= N; ++i) {
  for (int w = 1; w <= W; ++w) {
      printf("%d ", mvt[w][i]);
    }
    printf("\n");
  }
  printf("%d\n", ans);

printf("\ndynamic solution\n");
  ans = Solve_dynamic();
    for (int i = 1; i <= N; ++i) {
  for (int w = 1; w <= W; ++w) {
      printf("%d ", mvt2[w][i]);
    }
    printf("\n");
  }
  printf("%d\n", ans);
}

int Solve(int w, int i) {
  int ans;
  if (!i || !w) {
    ans = 0;
  } else if (w - weight[i] >= 0) {
    ans = MAX(value[i] + Solve(w - weight[i], i - 1), Solve(w, i - 1));
  } else {
    ans = Solve(w, i- 1);
  }
  // printf("Solve(%d, %d) = %d\n", w, i, ans);
  return ans;
}

int Solve_memoization(int w, int i) {
  int *val = &mvt[w][i];
  if (!*val) {
    if (!w || !i) {
      *val = 0;
    } else if (w - weight[i] >= 0) {
      *val = MAX (value[i] + Solve_memoization(w - weight[i], i - 1),
                  Solve_memoization(w, i - 1));
    } else *val = Solve_memoization(w, i - 1);
  }
  return *val;
}
int Solve_dynamic() {
  for (int w = 1; w <= W; ++w) {
    for (int i = 1; i <= N; ++i) {
      if (w - weight[i] >= 0) {
        mvt2[w][i] = MAX(value[i] + mvt2[w - weight[i]][i - 1], mvt2[w][i - 1]);
      } else mvt2[w][i] = mvt2[w][i - 1];
    }
  }
  return mvt2[W][N];
}
