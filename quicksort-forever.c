#include <stdio.h>
#define MAX_N 100000

unsigned int in[MAX_N];
int n;

void input() {
    scanf("%d", &n);
    for (int i = 0; i < n; ++i) {
        scanf("%u", &in[i]);
    }
}
void swap(unsigned int* a, unsigned int* b) {
    unsigned int temp = *a;
    *a = *b;
    *b = temp;
}
void swap_by_index(int x1, int x2) {
    swap(&in[x1], &in[x2]);
}
void quicksort(int l, int r) {
    if (l >= r)
        return;
    
    // Choose smaller ones to pivot to pass the boundery exception
    // which will occur later.
    if (in[l] > in[r]) {
        swap_by_index(l, r);
    }
    // let in[l] be the pivot
    unsigned int p = in[l];
    int l_i = l + 1;
    int r_i = r;
    // ll_i, rr_i stores elements same as pivot value.
    int ll_i = l;
    int rr_i = r + 1;
    while (1) {
        if (l_i >= r_i) {
            swap_by_index(l, l_i);
            ++l_i;
            break;
        } else {
            while (in[l_i] <= p) {
                if (in[l_i] == p) {
                    swap_by_index(l_i, ll_i++);
                } 
                ++l_i;
            }
            while (p <= in[r_i]) {
                if (in[r_i] == p) {
                    swap_by_index(r_i, rr_i--);
                }
                --r_i;
            }
            swap_by_index(l_i, r_i);
        }
    }
    // Gather pivot-same elements to center.
    while (ll_i >= l) {
        swap_by_index(ll_i--, r_i--);
    }
    while (rr_i <= r) {
        swap_by_index(rr_i++, l_i++);
    }
    quicksort(l, r_i);
    quicksort(l_i, r);
}
void print_array() {
    for (int i = 0; i < n; ++i) {
        printf("%d ", in[i]);
    }
    printf("\n");
}
int main() {
    input();
    quicksort(0, n - 1);
    print_array();
}
